package com.lebroncraft.minigame.item;


import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

import com.lebroncraft.core.common.util.Rand;
import com.lebroncraft.core.common.util.UtilMat;
import com.lebroncraft.core.common.util.UtilPlayer;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.minigame.game.Game;


public class SoupManager
		extends PluginModule {
		
	public boolean bowlDropDelete = true;
	
	public SoupManager(Game game) {
		super("Soup Manager", game);
		game.itemDropAllow.add(UtilMat.getData(Material.BOWL));
	}
	
	@EventHandler
	public void consumeSoup(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		if (!UtilPlayer.isHolding(player, Material.MUSHROOM_SOUP)) {
			return;
		}
		
		// double health = player.getHealth();
		// double maxHealth = player.getMaxHealth();
		//
		// if (health >= maxHealth) {
		// return;
		// }
		
		player.removePotionEffect(PotionEffectType.REGENERATION);
		player.addPotionEffect(PotionEffectType.REGENERATION.createEffect(100, 3));
		UtilSound.playPhysical(player.getLocation(), Sound.EAT, 1F, Rand.rFloat(1.1F, 1.3F));
		
		PlayerInventory inv = player.getInventory();
		final int slot = inv.getHeldItemSlot();
		
		runSyncDelayed(0L, new Runnable() {
			@Override
			public void run() {
				inv.clear(slot);
				player.updateInventory();
			}
		});
	}
	
	@EventHandler
	public void onBowlDrop(PlayerDropItemEvent event) {
		if (!bowlDropDelete) {
			return;
		}
		
		Item drop = event.getItemDrop();
		ItemStack item = drop.getItemStack();
		
		if (item.getType() == Material.BOWL) {
			drop.remove();
		}
		
	}
	
}
