package com.lebroncraft.minigame.kit;


import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.event.ClickEntityEvent;
import com.lebroncraft.core.mob.Mob;


public class MobKit
		extends Mob {
		
	private Kit kit;
	
	public MobKit(Kit kit, UUID uid, Location location) {
		super(uid, location);
		this.kit = kit;
	}
	
	@Override
	public void load(LivingEntity entity) {
		super.load(entity);
		
		UtilEntity.removeAI(entity);
		UtilEntity.setSilent(entity, true);
		
		kit.giveItems(entity.getEquipment());
		setArmourInvulnerable(true);
		
		String name = C.cBold + "Kit " + C.sDash + " " + C.cGreen + C.cBold + kit.getName();
		
		setTag(name);
	}
	
	@Override
	public void onClick(ClickEntityEvent event) {
		Player player = event.getClicker();
		kit.click(player, event.getClick());
	}
	
	public Kit getKit() {
		return kit;
	}
	
}
