package com.lebroncraft.minigame.kit;


import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.core.mob.MobType;


public class EmptyKit
		extends Kit {
		
	public EmptyKit(String name, String[] desc, MobType type) {
		super(name, desc);
		mobType = type;
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
	
	}
	
}
