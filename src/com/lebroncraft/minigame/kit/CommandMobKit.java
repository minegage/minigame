package com.lebroncraft.minigame.kit;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.mob.command.manager.CommandMobManager;
import com.lebroncraft.core.mob.command.manager.CreateToken;


public class CommandMobKit
		extends CommandMobManager<KitManager, MobKit> {
		
	public CommandMobKit(KitManager manager) {
		super(manager, "MobKit");
	}
	
	@Override
	public CreateToken newToken() {
		return new KitToken();
	}
	
	public static class KitToken
			extends CreateToken {
			
		private String kit;
		private Location location;
		
		@Override
		public boolean create(Player player, List<String> args, Flags flags) {
			if (args.size() < 1) {
				C.pMain(player, "Kit", "Please specify a kit name");
				return false;
			}
			
			this.kit = Util.joinList(args, " ");
			this.location = player.getLocation();
			
			return true;
		}
		
		@Override
		public String create(LivingEntity entity) {
			entity.setNoDamageTicks(Integer.MAX_VALUE);
			entity.teleport(location);
			UtilEntity.removeAI(entity);
			
			String uid = entity.getUniqueId()
					.toString();
			String loc = UtilPos.serializeLocation(location);
			
			return kit + ":" + uid + ":" + loc;
		}
		
	}
	
}
