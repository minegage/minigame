package com.lebroncraft.minigame.kit.attrib;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.util.UtilEvent;


public abstract class AttribItem
		extends Attrib {
		
	private Material[] types;
	
	public AttribItem(String name, String[] desc, Material... item) {
		super(name, desc);
		this.types = item;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public final void onInteract(PlayerInteractEvent event) {
		if (event.isCancelled()) {
			return;
		}
		
		if (!UtilEvent.isClick(event)) {
			return;
		}
		
		ItemStack clicked = event.getItem();
		if (!isItem(clicked)) {
			return;
		}
		
		event.setCancelled(true);
		
		Click click = Click.from(event);
		onClick(event.getPlayer(), clicked, click);
		
		event.getPlayer()
				.updateInventory();
	}
	
	public abstract void onClick(Player player, ItemStack item, Click click);
	
	public boolean isItem(ItemStack item) {
		if (item == null) {
			return false;
		}
		
		Material material = item.getType();
		
		for (Material mat : types) {
			if (material == mat) {
				return true;
			}
		}
		
		return false;
	}
	
}
