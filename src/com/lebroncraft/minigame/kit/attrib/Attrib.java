package com.lebroncraft.minigame.kit.attrib;


import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.kit.Descriptive;
import com.lebroncraft.minigame.kit.Kit;


/**
 * Represents an attribute of a kit. Usually is a potion effect, ability, etc.
 */
public abstract class Attrib
		extends Descriptive
		implements Listener {
		
	protected Set<Player> applied = new HashSet<>();
	protected Kit kit;
	
	// If false, appliesTo() will return false if the game is explaining
	protected boolean explainUse = true;
	
	public Attrib(String name, String... desc) {
		super(C.cBold + name, desc);
	}
	
	public void setKit(Kit kit) {
		this.kit = kit;
	}
	
	public abstract void apply(Player player);
	
	protected Game getGame() {
		return kit.getGame();
	}
	
	/**
	 * @return If this attribute is applicable to the player
	 */
	protected boolean appliesTo(Player player) {
		Game game = getGame();
		
		if (game == null || !game.isPlaying()) {
			return false;
		}
		
		if (!explainUse && game.explaining) {
			return false;
		}
		
		if (!game.canInteract(player)) {
			return false;
		}
		
		if (!game.getKit(player)
				.equals(this.kit)) {
			return false;
		}
		
		return true;
	}
	
	
	
}
