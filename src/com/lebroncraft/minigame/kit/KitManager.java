package com.lebroncraft.minigame.kit;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import com.lebroncraft.core.block.BlockTracker;
import com.lebroncraft.core.common.util.UtilBlock;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.team.GameTeam;


public class KitManager
		extends MobManager<MobKit> {
		
	public static final String FILE_NAME = "mobkits.txt";
	
	private BlockTracker blockTracker = new BlockTracker();
	private GameManager gameManager;
	
	private final Vector centreKitPos = new Vector(0, 25, 8);
	private final Vector kitIterateDir = UtilPos.fromBlockFace(BlockFace.WEST);
	private final Vector facingDir = UtilPos.fromBlockFace(BlockFace.NORTH);
	private final int spacing = 4;
	private final int teamSpacing = 6;
	
	public KitManager(GameManager gameManager) {
		super("Kit Manager", gameManager.getPlugin(), FILE_NAME);
		this.gameManager = gameManager;
		
		registerCommand(new CommandMobKit(this));
	}
	
	public MobKit create(Kit kit, MobType type, Location location) {
		LivingEntity entity = type.spawn(location);
		MobKit mobKit = new MobKit(kit, entity.getUniqueId(), location);
		
		mobKit.load(entity);
		
		if (!mobKit.isPostLoaded()) {
			mobKit.unload();
		}
		
		return mobKit;
	}
	
	@Override
	public String serializeMob(MobKit mob) {
		String name = mob.getKit()
				.getName();
		String loc = UtilPos.serializeLocation(mob.getPostLocation());
		
		return name + ":" + loc;
	}
	
	@Override
	public MobKit deserializeMob(World world, String serialized) {
		String[] split = serialized.split(":");
		
		String kitName = split[0];
		Location location = UtilPos.deserializeLocation(split[1], world);
		
		Game game = gameManager.getGame();
		Kit kit = game.getKit(kitName);
		
		if (kit == null) {
			L.warn("Invalid serialized mob kit; kit \"" + kitName + "\" is not defined. Skipping entity at " + UtilPos.format(location));
		}
		
		return create(kit, kit.mobType, location);
	}
	
	/**
	 * @param loc
	 *        The middle location of the kit stand
	 */
	public void createKitStand(Kit kit, Location location) {
		// Bottom layer
		for (Vector vec : blocksLayer) {
			Location relLoc = location.clone()
					.add(vec)
					.add(0, -1, 0);
			setBlock(relLoc, 5, (byte) 1);
		}
		
		// Middle layer
		int faceIndex = 0;
		for (Vector vec : blocksLayer) {
			Location relLoc = location.clone()
					.add(vec);
					
			BlockFace face = stairFaces.get(faceIndex++);
			byte data = UtilBlock.getStairFacing(face);
			data = UtilBlock.invertStair(data);
			
			setBlock(relLoc, 134, data);
		}
		
		// Top layer
		for (Vector vec : blocksLayer) {
			Location relLoc = location.clone()
					.add(vec)
					.add(0, 1, 0);
			setBlock(relLoc, 126, (byte) 2);
		}
		
		Location mobLoc = location.add(0, 1.5, 0);
		
		mobLoc.setDirection(facingDir);
		MobType mobType = kit.getMobType();
		
		MobKit mobKit = create(kit, mobType, location);
		registerMob(mobKit);
	}
	
	public void buildLobbyKitStands() {
		Game game = gameManager.getGame();
		
		Location base = centreKitPos.toLocation(UtilWorld.getMainWorld());
		Vector offsetDir = kitIterateDir.clone()
				.multiply(-1);
				
		if (game.teamUniqueKits) {
			List<GameTeam> teams = new ArrayList<>(game.getTeams());
			teams.remove(game.getSpectatorTeam());
			
			int totalTeams = teams.size();
			int totalKits = teams.stream()
					.mapToInt(team -> team.getKits()
							.size())
					.sum();
					
			// Leftmost kit location
			// Subtract 2 to get total distance between the middle of the left/rightmost kits
			int totalWidth = 2 * ( totalKits - 1 ) + teamSpacing * ( totalTeams - 1 ) - 2;
			
			Location baseLoc = base.add(offsetDir.clone()
					.multiply(totalWidth / 2));
					
			Vector offsetAdd = kitIterateDir.clone();
			
			for (int teamNum = 0; teamNum < teams.size(); teamNum++) {
				GameTeam team = teams.get(teamNum);
				List<Kit> kits = team.getKits();
				
				buildKits(baseLoc.clone(), kits);
				
				// Add kit offset + team spacing to get leftmost kit location of next team
				int offsetLen = 2 * ( kits.size() - 1 );
				baseLoc.add(offsetAdd.clone()
						.multiply(offsetLen + teamSpacing));
			}
			
		} else {
			List<Kit> kits = game.getGlobalKits();
			int offsetLen = 2 * ( kits.size() - 1 );
			Vector offset = offsetDir.multiply(offsetLen);
			
			Location buildLoc = base.add(offset);
			buildKits(buildLoc, kits);
		}
	}
	
	public void buildKits(Location iterStart, List<Kit> kits) {
		for (int kitNum = 0; kitNum < kits.size(); kitNum++) {
			Vector offsetAdd = kitIterateDir.clone()
					.multiply(kitNum * spacing);
					
			Location kitLoc = iterStart.clone()
					.add(offsetAdd);
					
			createKitStand(kits.get(kitNum), kitLoc);
		}
		
	}
	
	public void restore() {
		blockTracker.restoreAll();
		clearMobs(UtilWorld.getMainWorld());
	}
	
	private void setBlock(Location location, int type, byte data) {
		blockTracker.track(location.getBlock());
		UtilBlock.set(location, type, data);
	}
	
	private static List<Vector> blocksLayer = new ArrayList<>();
	private static List<BlockFace> stairFaces = new ArrayList<>();
	
	static {
		/* 2x2 X/Z grid of relative block positions for kit stands */
		blocksLayer.add(new Vector(0.5, 0, 0.5)); // Top right
		blocksLayer.add(new Vector(0.5, 0, -0.5)); // Bottom right
		blocksLayer.add(new Vector(-0.5, 0, -0.5)); // Bottom left
		blocksLayer.add(new Vector(-0.5, 0, 0.5)); // Top left
		
		/**
		 * The directions for stairs being placed on the stand. Stairs should be placed in this
		 * order at the relative block location with the same index
		 */
		stairFaces.add(BlockFace.NORTH);
		stairFaces.add(BlockFace.WEST);
		stairFaces.add(BlockFace.SOUTH);
		stairFaces.add(BlockFace.EAST);
	}
	
}
