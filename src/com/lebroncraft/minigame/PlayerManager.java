package com.lebroncraft.minigame;


import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilPlayer;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;
import com.lebroncraft.minigame.game.Game.PlayerState;
import com.lebroncraft.minigame.game.event.GameStateChangeEvent;
import com.lebroncraft.minigame.team.GameTeam;


public class PlayerManager
		extends PluginModule {
		
	private MinigameManager manager;
	
	public PlayerManager(MinigameManager manager) {
		super("Player Manager", manager);
		this.manager = manager;
	}
	
	public Game getGame() {
		return manager.getGame();
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void loadWorld(WorldLoadEvent event) {
		World world = event.getWorld();
		world.setDifficulty(Difficulty.HARD);
	}
	
	@EventHandler
	public void setJoinLocation(PlayerSpawnLocationEvent event) {
		Game game = getGame();
		Player player = event.getPlayer();
		
		if (game == null || !game.isPlaying() || !game.canInteract(player) || game.getMap() == null) {
			event.setSpawnLocation(UtilWorld.getMainWorld()
					.getSpawnLocation());
		} else {
			event.setSpawnLocation(game.getMap()
					.getSpawnLocation());
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void joinGame(PlayerJoinEvent event) {
		Game game = getGame();
		if (game == null) {
			return;
		}
		
		Player player = event.getPlayer();
		UtilPlayer.reset(player);
		
		PlayerState state;
		if (game.inLobby()) {
			state = PlayerState.IN;
		} else if (!game.joinOut) {
			state = PlayerState.IN;
			game.assignTeam(player);
		} else {
			state = PlayerState.OUT;
			game.out(player, false);
		}
		
		if (game.teamUniqueKits) {
			for (GameTeam team : game.getTeams()) {
				if (team.isVisible()) {
					team.defaultKit.select(player, false);
				}
			}
		} else {
			game.defaultKit.select(player, false);
		}
		
		game.setState(player, state);
		UtilUI.sendTabText(player, head, foot);
		
		game.respawn(player);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void quitGame(PlayerQuitEvent event) {
		Game game = getGame();
		if (game != null) {
			game.clearReference(event.getPlayer());
		}
	}
	
	private String head;
	private String foot = C.cBold + "Check out " + C.sOut + C.cBold + "lebroncraft.com" + C.cWhite + C.cBold
			+ " for shop, forums, and more!";
			
	@EventHandler
	public void updateHead(GameStateChangeEvent event) {
		Game game = getGame();
		if (game == null) {
			return;
		}
		
		if (event.getNewState() != GameState.LOADING) {
			return;
		}
		
		head = UtilUI.getServerDisplay(game.getName());
		
		for (Player player : UtilServer.players()) {
			UtilUI.sendTabText(player, head, foot);
		}
	}
	
	
}
