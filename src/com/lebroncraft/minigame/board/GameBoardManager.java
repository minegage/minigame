package com.lebroncraft.minigame.board;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.BoardManager;
import com.lebroncraft.core.board.objective.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;
import com.lebroncraft.minigame.game.Game.PlayerState;
import com.lebroncraft.minigame.game.event.PlayerStateChangeEvent;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.team.JoinTeamEvent;


/**
 * Board manager which is used during the actual gameplay of the game. This class will not be
 * enabled if the game state is not {@link GameState#PLAYING} or {@link GameState#ENDING}
 */
public class GameBoardManager
		extends BoardManager {
		
	protected Game game;
	
	public GameBoardManager(Game game) {
		super(game.getPlugin());
		this.game = game;
	}
	
	public Board create() {
		Board board = new Board();
		createGameTeams(board);
		
		return board;
	}
	
	// Called through game manager, or when a player joins
	public void giveBoard(Player player) {
		Board board = getBoard(player);
		board = create();
		setBoard(player, board);
		
		ObjectiveSide side = board.setSideObjective();
		side.setHeader(C.cGreen + C.cBold + game.getName());
		
		GameTeam gameTeam = game.getTeam(player);
		setDisplayName(player, gameTeam);
		
		game.giveBoard(player, board);
	}
	
	@EventHandler
	public void setDisplay(JoinTeamEvent event) {
		setDisplayName(event.getPlayer(), event.getTeam());
	}
	
	public void setDisplayName(Player player, GameTeam gameTeam) {
		Board board = getBoard(player);
		
		// Board will be null when a player is joining mid-game
		if (board == null) {
			return;
		}
		
		Team team = board.getBoard()
				.getTeam(gameTeam.getName());
				
		String prefix = team.getPrefix();
		if (game.getState(player) == PlayerState.OUT) {
			prefix = prefix + "DEAD ";
		}
		
		player.setDisplayName(prefix + player.getName() + C.cReset);
		
		for (Board other : getPlayerBoards()) {
			Team otherTeam = other.getBoard()
					.getTeam(gameTeam.getName());
			otherTeam.addPlayer(player);
		}
	}
	
	// Low priority so that the player will be assigned a team, given spectator, etc. before the
	// board is given.
	@EventHandler(priority = EventPriority.LOW)
	public void giveBoard(PlayerJoinEvent event) {
		giveBoard(event.getPlayer());
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onOut(PlayerStateChangeEvent event) {
		if (event.getNewState() == PlayerState.OUT) {
			Player player = event.getPlayer();
			
			GameTeam team = game.getTeam(player);
			
			String prefix = C.cGray + "DEAD " + team.getPrefix();
			
			player.setDisplayName(prefix + player.getName() + C.cReset);
		}
	}
	
	public void createGameTeams(Board board) {
		Scoreboard scoreboard = board.getBoard();
		
		for (GameTeam gameTeam : game.getTeams()) {
			String name = gameTeam.getName();
			
			Team team = scoreboard.registerNewTeam(name);
			String prefix = gameTeam.getPrefix();
			team.setPrefix(prefix);
			
			for (Player player : gameTeam.getPlayers()) {
				team.addPlayer(player);
			}
		}
	}
	
}
