package com.lebroncraft.minigame;


import com.lebroncraft.core.block.BlockManager;
import com.lebroncraft.core.combat.CombatManager;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.stats.StatManager;
import com.lebroncraft.minigame.command.CommandDebug;
import com.lebroncraft.minigame.command.CommandDie;
import com.lebroncraft.minigame.command.CommandStat;
import com.lebroncraft.minigame.command.game.CommandGame;
import com.lebroncraft.minigame.command.state.CommandState;
import com.lebroncraft.minigame.event.GameEventManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.item.ItemManager;
import com.lebroncraft.minigame.lobby.LobbyManager;
import com.lebroncraft.minigame.map.MapManager;


public class MinigameManager
		extends PluginModule {
		
	private PlayerManager playerManager;
	private ItemManager itemManager;
	
	private MenuManager menuManager;
	private StatManager statManager;
	private MapManager mapManager;
	
	private GameManager gameManager;
	private GameEventManager eventManager;
	private LobbyManager lobbyManager;
	
	private CombatManager combatManager;
	private BlockManager blockManager;
	
	public MinigameManager(Minigame minigame) {
		super("Minigame Manager", minigame);
		
		this.playerManager = new PlayerManager(this);
		this.itemManager = new ItemManager(this);
		
		this.menuManager = new MenuManager(plugin);
		this.statManager = new StatManager(plugin);
		this.mapManager = new MapManager(plugin);
		
		this.combatManager = new CombatManager(plugin);
		this.blockManager = new BlockManager(plugin);
		
		this.gameManager = new GameManager(this);
		this.eventManager = new GameEventManager(this);
		this.lobbyManager = new LobbyManager(gameManager);
		
		registerCommand(new CommandDebug(this));
		registerCommand(new CommandDie());
		registerCommand(new CommandGame(gameManager));
		registerCommand(new CommandState(gameManager));
		registerCommand(new CommandStat(gameManager));
	}
	
	public Game getGame() {
		return getGameManager().getGame();
	}
	
	public Minigame getMinigame() {
		return (Minigame) getPlugin();
	}
	
	public MapManager getMapManager() {
		return mapManager;
	}
	
	public StatManager getStatManager() {
		return statManager;
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
	public GameEventManager getEventManager() {
		return eventManager;
	}
	
	public LobbyManager getLobbyManager() {
		return lobbyManager;
	}
	
	public MenuManager getMenuManager() {
		return menuManager;
	}
	
	public CombatManager getCombatManager() {
		return combatManager;
	}
	
	public BlockManager getFallingBlockManager() {
		return blockManager;
	}
	
	public ItemManager getItemManager() {
		return itemManager;
	}
	
	public PlayerManager getPlayerManager() {
		return playerManager;
	}
	
}
