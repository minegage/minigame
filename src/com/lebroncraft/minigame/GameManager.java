package com.lebroncraft.minigame;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.google.common.collect.Lists;
import com.lebroncraft.core.block.BlockManager;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.Note;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilMath;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.common.util.UtilTime;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.event.CustomDeathEvent;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;
import com.lebroncraft.minigame.game.GameToken;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.game.event.GameStateChangeEvent;
import com.lebroncraft.minigame.kit.KitManager;
import com.lebroncraft.minigame.lobby.LobbyManager;
import com.lebroncraft.minigame.map.MapManager;


public class GameManager
		extends PluginModule {
		
	public static final String FILE_NAME = "rotation.txt";
	
	private static final double LOAD_EXPIRE = 30.0;
	private static final double END_TIME = 12.0;
	
	private MinigameManager minigameManager;
	private RuleManager ruleManager;
	private KitManager kitManager;
	
	private Game game;
	
	public boolean start = false;
	public boolean tick = true;
	public String mapName;
	
	private List<GameType> gameRotation = Lists.newArrayList();
	
	// Index corresponds to how many games ago the map was played
	private LinkedList<GameToken> gameHistory = new LinkedList<>();
	
	// Maximum size of game history
	private final int HISTORY_SIZE = 10;
	
	public GameManager(MinigameManager minigameManager) {
		super("Game Manager", minigameManager);
		
		this.minigameManager = minigameManager;
		
		reloadRotations();
		
		this.ruleManager = new RuleManager(this);
		this.kitManager = new KitManager(this);
		
		runSyncDelayed(0L, new Runnable() {
			@Override
			public void run() {
				createNextGame();
			}
		});
		
	}
	
	/**
	 * Reloads game and map rotation from disk
	 */
	public void reloadRotations() {
		gameRotation.clear();
		
		try {
			File rotationFile = new File(Bukkit.getWorldContainer(), FILE_NAME);
			rotationFile.createNewFile();
			
			List<String> gameTypes = FileUtils.readLines(rotationFile);
			
			for (String type : gameTypes) {
				GameType gameType = UtilJava.parseEnum(GameType.class, type);
				if (gameType == null) {
					L.warn("Invalid game type \"" + type + "\" in rotation list; skipping");
				} else {
					gameRotation.add(gameType);
				}
			}
			
			if (gameRotation.size() == 0) {
				L.warn("No games in rotation!");
			}
			
			String rotation = "";
			
			Iterator<GameType> gamesIt = gameRotation.iterator();
			if (!gamesIt.hasNext()) {
				rotation = "No games!";
			}
			
			while (gamesIt.hasNext()) {
				rotation += gamesIt.next()
						.getName();
				if (gamesIt.hasNext()) {
					rotation += ", ";
				}
			}
			
			L.info("Loaded game rotation: [" + rotation + "]" + " (" + gameRotation.size() + ")");
			
		} catch (IOException ex) {
			L.error(ex, "Unable to load game rotation");
		}
		
		getMapManager().loadMapRotation();
	}
	
	/**
	 * Behaviour for state changing based on other factors (player count, time passed, etc)
	 */
	@EventHandler
	public void tickState(TickEvent event) {
		if (!tick || event.getTick() != Tick.SEC_1) {
			return;
		}
		
		Game game = getGame();
		
		if (game == null) {
			return;
		}
		
		GameState state = game.getState();
		
		int online = UtilServer.numPlayers();
		int playing = game.getPlayersIn()
				.size();
				
		double stateSeconds = game.getStateSeconds();
		
		if (state == GameState.LOADING) {
			if (stateSeconds >= LOAD_EXPIRE) {
				L.severe("Game loading expired");
				game.setState(GameState.DEAD);
			}
			
		} else if (state == GameState.WAITING) {
			
			if (online >= game.minPlayers) {
				game.setState(GameState.STARTING);
			}
			
		} else if (state == GameState.STARTING) {
			
			// Reset game if there's not enough players
			if (online < game.minPlayersAbsolute) {
				game.skippedWaiting = false;
				game.setState(GameState.WAITING);
			} else if (!game.skippedWaiting && online >= game.maxPlayers) {
				game.setTicks(UtilTime.toTicks(LobbyManager.SKIP_SECONDS));
				game.skippedWaiting = true;
			} else if (stateSeconds >= LobbyManager.START_SECONDS) {
				game.setState(GameState.PLAYING);
			} else if (!game.paused && LobbyManager.START_SECONDS - stateSeconds < 10) {
				UtilSound.playGlobal(Sound.NOTE_PLING, 1F, 1F);
			}
			
		} else if (state == GameState.PLAYING) {
			
			if (!game.explaining && game.endCheck()) {
				game.setState(GameState.ENDING);
			} else if (game.timed && game.getRemainingSeconds() <= 0) {
				game.setState(GameState.ENDING);
			} else if (playing < game.minPlayersAbsolute) {
				C.bWarn("Game", "Not enough players are online to play. Please wait for more players.");
				game.setState(GameState.DEAD);
			}
			
		} else if (state == GameState.ENDING) {
			if (stateSeconds > END_TIME) {
				game.setState(GameState.DEAD);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void deathEndCheck(CustomDeathEvent event) {
		if (event.isPlayerKilled()) {
		
		}
	}
	
	/**
	 * Determines what happens when the game state is changed. Monitor priority is used so that
	 * states are changed last, and by extension state change events with a lower priority happen in
	 * the correct order
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void handleStateChange(GameStateChangeEvent event) {
		GameState newState = event.getNewState();
		
		if (newState == GameState.LOADING) {
			// Reload map rotation
			getMapManager().loadMapRotation();
			
			GameType type = game.getType();
			
			try {
				game.load(mapName);
				
				if (game.lobby) {
					game.setState(GameState.WAITING);
				} else {
					game.setState(GameState.PLAYING);
				}
				
			} catch (IOException ex) {
				L.error(ex, "Unable to load map \"" + mapName + "\" for " + type.getName());
			}
		} else if (newState == GameState.PLAYING) {
			for (Player player : UtilServer.players()) {
				game.assignTeam(player);
				game.respawn(player);
			}
			
			game.start();
			
			if (game.explain) {
				int freezeTicks = UtilTime.toTicks(game.explainTime);
				int startTicks = UtilServer.currentTick();
				
				runSyncTimer("explain tick", 0L, 1L, new Runnable() {
					int tick = 0;
					
					@Override
					public void run() {
						tick++;
						
						int passedTicks = UtilServer.currentTick() - startTicks;
						double secondsPassed = UtilTime.toSeconds(passedTicks);
						
						double displaySeconds = UtilMath.round(game.explainTime - secondsPassed, 1);
						String message = C.cBold + "Game starts in " + C.sOut + C.cBold + displaySeconds + "s" + C.cWhite
								+ C.cBold + "...";
								
						if (tick > freezeTicks) {
							UtilSound.playGlobal(Sound.NOTE_PLING, 1F, Note.O3_D);
							cancelSyncTask("explain tick");
							game.explaining = false;
						} else {
							if (displaySeconds % 1 == 0) {
								UtilSound.playGlobal(Sound.NOTE_STICKS, 1F, 1.2F);
							}
						}
						
						for (Player player : UtilServer.players()) {
							UtilUI.sendActionBar(player, message);
						}
					}
				});
			}
			
			for (Player player : UtilServer.players()) {
				game.getBoardManager()
						.giveBoard(player);
			}
			
		} else if (newState == GameState.ENDING) {
			game.end();
		} else if (newState == GameState.DEAD) {
			// Unload and delete all worlds except for main
			
			killGame();
			createNextGame();
		}
	}
	
	public List<String> getMapHistory() {
		return gameHistory.stream()
				.map(token -> token.map)
				.collect(Collectors.toList());
	}
	
	public List<String> getMapHistory(GameType type) {
		return gameHistory.stream()
				.filter(token -> ( token.type == type ))
				.map(token -> token.map)
				.collect(Collectors.toList());
	}
	
	public List<GameType> getGameHistory() {
		return gameHistory.stream()
				.map(token -> token.type)
				.collect(Collectors.toList());
	}
	
	public void killGame() {
		World main = UtilWorld.getMainWorld();
		
		for (World world : Bukkit.getWorlds()) {
			if (UtilWorld.isMainWorld(world)) {
				continue;
			}
			
			for (Player player : world.getPlayers()) {
				player.teleport(main.getSpawnLocation());
			}
			
			UtilWorld.forceUnload(world, false);
		}
		
		for (String world : UtilWorld.getUnloadedWorlds()) {
			if (UtilWorld.isMainWorld(world)) {
				continue;
			}
			
			try {
				UtilWorld.delete(world);
			} catch (IOException ex) {
				L.error(ex, "Unable to delete world \"" + world + "\"");
			}
		}
		
		if (game != null) {
			game.disable();
			game = null;
		}
		
		this.mapName = null;
	}
	
	/**
	 * Creates a game of the least played gametype, on the least played map
	 */
	public void createNextGame() {
		GameType type = nextGameType();
		
		mapName = nextMap(type);
		createGame(type, mapName);
	}
	
	public void createGame(GameType type) {
		createGame(type, nextMap(type));
	}
	
	public void createGame(GameType type, String map) {
		if (game != null) {
			killGame();
		}
		
		game = initGame(type, map);
		if (game == null) {
			return;
		}
		
		game.setState(GameState.LOADING);
		kitManager.restore();
		kitManager.buildLobbyKitStands();
	}
	
	private String nextMap(GameType type) {
		List<String> maps = Lists.newArrayList(getMapManager().getMapRotation()
				.get(type));
				
		if (maps.isEmpty()) {
			L.severe("No maps in rotation; cannot create game");
			return null;
		}
		
		List<String> prevMaps = getMapHistory(type);
		maps.sort((str1, str2) -> Integer.compare(prevMaps.indexOf(str1), prevMaps.indexOf(str2)));
		
		mapName = maps.get(0);
		return mapName;
	}
	
	/**
	 * Instantiates a Game of the given type using the given map
	 */
	private Game initGame(GameType type, String map) {
		Class<? extends Game> clazz = type.getClazz();
		
		try {
			Constructor<? extends Game> struct = clazz.getConstructor(MinigameManager.class);
			struct.setAccessible(true);
			
			Game game = struct.newInstance(minigameManager);
			
			gameHistory.addFirst(new GameToken(type, map));
			if (gameHistory.size() > HISTORY_SIZE) {
				gameHistory.removeLast();
			}
			
			return game;
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException ex) {
			L.error(ex, "Failed to instantiate game");
			throw new Error("Unresolved compilation problem: \n\tGame instantiation failed; A " + ex.getClass()
					.getSimpleName() + " occurred when instantiating " + clazz.getName());
		} catch (InvocationTargetException ex) {
			Throwable target = ex.getTargetException();
			L.error(target, "Failed to instantiate game");
			return null;
		}
	}
	
	private GameType nextGameType() {
		if (gameHistory.size() == 0) {
			return UtilJava.getRandIndex(gameRotation);
		}
		
		GameType lastType = gameHistory.get(0).type;
		int nextIndex = gameRotation.indexOf(lastType) + 1;
		
		return UtilJava.getWrappedIndex(gameRotation, nextIndex);
	}
	
	public List<GameType> getGameRotation() {
		return gameRotation;
	}
	
	public Game getGame() {
		return game;
	}
	
	public MinigameManager getMinigameManager() {
		return minigameManager;
	}
	
	public RuleManager getRuleManager() {
		return ruleManager;
	}
	
	public Minigame getMinigame() {
		return getMinigameManager().getMinigame();
	}
	
	public MapManager getMapManager() {
		return getMinigameManager().getMapManager();
	}
	
	public BlockManager getFallingBlockManager() {
		return getMinigameManager().getFallingBlockManager();
	}
	
}

