package com.lebroncraft.minigame.team;


import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class JoinTeamEvent
		extends Event {
		
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	private Player player;
	private GameTeam team;
	
	public JoinTeamEvent(Player player, GameTeam team) {
		this.player = player;
		this.team = team;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public GameTeam getTeam() {
		return team;
	}
	
}
