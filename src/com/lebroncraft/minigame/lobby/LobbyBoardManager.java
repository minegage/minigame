package com.lebroncraft.minigame.lobby;


import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.BoardManager;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;
import com.lebroncraft.minigame.game.event.GameStateChangeEvent;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.kit.SelectKitEvent;


/**
 * Handles lobby scoreboard. Is only loaded when the game is in the lobby.
 * 
 * @see com.lebroncraft.minigame.lobby.LobbyManager#handleLobbyBoard(GameStateChangeEvent)
 */
public class LobbyBoardManager
		extends BoardManager {
		
	private LobbyManager lobbyManager;
	
	// Placeholders
	private String header = "";
	private String playerCount = "";
	private String status = "";
	private String map = "";
	
	public LobbyBoardManager(LobbyManager manager) {
		super(manager.getPlugin());
		this.lobbyManager = manager;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		for (Board board : getPlayerBoards()) {
			UtilUI.assignRankTeams(board);
		}
		
		giveBoard(player);
	}
	
	public void giveBoard(Player player) {
		LobbyBoard board = new LobbyBoard(header, status, playerCount, map);
		setBoard(player, board);
	}
	
	@Override
	protected void onEnable() {
		for (Player player : UtilServer.players()) {
			giveBoard(player);
		}
	}
	
	@Override
	protected void onDisable() {
		for (Player player : UtilServer.players()) {
			removeBoard(player);
		}
	}
	
	@EventHandler
	public void updateMap(GameStateChangeEvent event) {
		if (event.getNewState() == GameState.WAITING) {
			
			updateHeader();
			updateStatus();
			updateCount();
			updateMap();
		}
	}
	
	@EventHandler
	public void onKitSelect(SelectKitEvent event) {
		Player player = event.getPlayer();
		Kit kit = event.getKit();
		
		String name = kit.getName();
		getBoard(player).getSideObjective()
				.updateRow(LobbyBoard.ROW_KIT, name);
	}
	
	// Used to make the player count and status flash red/white
	boolean countTick = true;
	
	private String getCountColour() {
		return ( countTick ) ? C.cAqua : C.cWhite;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void updateStatus(TickEvent event) {
		if (event.getTick() != Tick.SEC_1) {
			return;
		}
		
		// Toggle between colours
		countTick = !countTick;
		
		updateCount();
		updateStatus();
	}
	
	public void updateStatus() {
		Game game = getGame();
		
		// Changes the status depending on game state
		if (game == null) {
			status = "Loading game...";
		} else {
			GameState state = game.getState();
			
			if (state == GameState.STARTING) {
				double stateSeconds = game.getStateSeconds();
				status = "Starting in " + (int) ( LobbyManager.START_SECONDS - stateSeconds );
			} else {
				status = getCountColour() + "Waiting for players";
			}
		}
		
		for (Board board : getPlayerBoards()) {
			board.getSideObjective()
					.updateRow(LobbyBoard.ROW_STATUS, status);
		}
	}
	
	
	public void updateCount() {
		Game game = getGame();
		if (game == null) {
			return;
		}
		
		int onlinePlayers = UtilServer.numPlayers();
		
		int min = game.minPlayers;
		int max = game.maxPlayers;
		
		String colour = "";
		if (onlinePlayers < min) {
			colour = getCountColour();
		}
		
		playerCount = colour + onlinePlayers + "/" + max;
		
		for (Board board : getPlayerBoards()) {
			board.getSideObjective()
					.updateRow(LobbyBoard.ROW_PLAYERS, playerCount);
		}
	}
	
	public void updateHeader() {
		Game game = getGame();
		if (game == null) {
			return;
		}
		
		header = C.cGreen + C.cBold + game.getName();
		for (Board board : getPlayerBoards()) {
			board.getSideObjective()
					.setHeader(header);
		}
	}
	
	public void updateMap() {
		World map = getGame().getMap();
		this.map = getLobbyManager().getGameManager()
				.getMapManager()
				.getData(map).name;
				
		for (Board board : getPlayerBoards()) {
			board.getSideObjective()
					.updateRow(LobbyBoard.ROW_MAP, this.map);
			;
		}
		
	}
	
	public LobbyManager getLobbyManager() {
		return lobbyManager;
	}
	
	public Game getGame() {
		return lobbyManager.getGameManager()
				.getGame();
	}
	
	
	
}
