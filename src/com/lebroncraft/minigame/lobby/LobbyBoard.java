package com.lebroncraft.minigame.lobby;


import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.objective.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilUI;


public class LobbyBoard
		extends Board {
		
	public static int ROW_STATUS;
	public static int ROW_PLAYERS;
	public static int ROW_KIT;
	public static int ROW_MAP;
	
	public LobbyBoard(String header, String status, String playerCount, String map) {
		UtilUI.createRankTeams(this);
		UtilUI.assignRankTeams(this);
		
		ObjectiveSide side = setSideObjective();
		side.setHeader(header);
		
		side.addRow("");
		side.addRow(C.cGreen + C.cBold + "Status");
		ROW_STATUS = side.addRow(status);
		side.addRow("");
		side.addRow(C.cYellow + C.cBold + "Players");
		ROW_PLAYERS = side.addRow(playerCount);
		side.addRow("");
		side.addRow(C.cGold + C.cBold + "Kit");
		ROW_KIT = side.addRow("None");
		side.addRow("");
		side.addRow(C.cRed + C.cBold + "Map");
		ROW_MAP = side.addRow(map);
		side.addRow("");
	}
	
}
