package com.lebroncraft.minigame.lobby;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.common.util.UtilZip;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.event.GameStateChangeEvent;
import com.lebroncraft.minigame.map.MapManager;


public class LobbyManager
		extends PluginModule {
		
	public static final int START_SECONDS = 10;
	public static final int SKIP_SECONDS = 5;
	
	private GameManager gameManager;
	private LobbyBoardManager boardManager;
	
	public LobbyManager(GameManager gameManager) {
		super("Lobby Manager", gameManager);
		
		this.gameManager = gameManager;
		this.boardManager = new LobbyBoardManager(this);
		
		setupLobby();
	}
	
	/**
	 * @param event
	 *        Documented to prevent warning
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void handleLobbyBoard(GameStateChangeEvent event) {
		Game game = gameManager.getGame();
		
		if (game.inLobby()) {
			if (!boardManager.isEnabled()) {
				boardManager.enable();
			}
		} else {
			if (boardManager.isEnabled()) {
				boardManager.disable();
			}
		}
	}
	
	public void setupLobby() {
		logInfo("Setting up lobby...");
		File serverDir = Bukkit.getWorldContainer();
		
		logInfo("Deleting all worlds...");
		
		// All worlds are unloaded at this point (STARTUP load time)
		List<String> worlds = UtilWorld.getUnloadedWorlds();
		
		if (worlds.size() > 5) {
			L.warn("There are more than 5 worlds on this server. This is a failsafe to prevent worlds from accidentally "
					+ "being deleted, should this plugin be uploaded to the wrong server. Please delete the worlds manually");
			return;
		}
		
		for (String worldName : UtilWorld.getUnloadedWorlds()) {
			File worldDir = new File(serverDir, worldName);
			
			try {
				FileUtils.forceDelete(worldDir);
			} catch (IOException ex) {
				L.error(ex, "Unable to delete world");
			}
		}
		
		
		// Unzip lobby to world directory
		logInfo("Extracting lobby zip...");
		MapManager manager = getGameManager().getMapManager();
		File worldsDir = manager.getWorldsDir();
		
		File lobbyZip = new File(worldsDir, "lobby.zip");
		
		if (!lobbyZip.exists()) {
			L.severe("Lobby file \"" + lobbyZip.getAbsolutePath() + "\" not found");
			return;
		}
		
		File lobbyDir = new File(serverDir, "world");
		try {
			UtilZip.extract(lobbyZip, lobbyDir);
		} catch (IOException ex) {
			L.error(ex, "Unable to extract lobby");
		}
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
	public LobbyBoardManager getBoardManager() {
		return boardManager;
	}
}
