package com.lebroncraft.minigame.map;


import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilString;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.GameType;


public class MapManager
		extends PluginModule {
		
	public static final String WORLDS_PATH = System.getProperty("user.home") + File.separator + "assets" + File.separator + "worlds";
	public static final String MAPS_PATH = System.getProperty("user.home") + File.separator + "assets" + File.separator + "maps";
	
	private final String BOUNDS_TASK = "bound check";
	
	public File getMapsDir() {
		return new File(MAPS_PATH);
	}
	
	public File getWorldsDir() {
		return new File(WORLDS_PATH);
	}
	
	private SetMultimap<GameType, String> mapRotation = HashMultimap.create();
	
	private SafeMap<World, DataMap> data = new SafeMap<>();
	private GameManager gameManager;
	
	public MapManager(JavaPlugin plugin) {
		super("Map Manager", plugin);
	}
	
	@Override
	protected void onEnable() {
		L.info("testing assets");
		L.info("world path '" + WORLDS_PATH + "'");
		L.info("map path '" + MAPS_PATH + "'");
		
		for (World world : Bukkit.getWorlds()) {
			loadData(world);
		}
		
		runBoundsCheck();
	}
	
	@Override
	protected void onDisable() {
		data.clear();
	}
	
	public void runBoundsCheck() {
		runSyncTimer(BOUNDS_TASK, 20L, 20L, new Runnable() {
			@Override
			public void run() {
				checkBounds();
			}
		});
	}
	
	public void cancelBoundsCheck() {
		cancelSyncTask(BOUNDS_TASK);
	}
	
	public void loadMapRotation() {
		mapRotation.clear();
		
		L.info("Loading map rotation...");
		
		File mapsDir = getMapsDir();
		mapsDir.mkdirs();
		
		for (File gameType : mapsDir.listFiles()) {
			if (!gameType.isDirectory()) {
				continue;
			}
			
			String gameName = gameType.getName();
			GameType type = UtilJava.parseEnum(GameType.class, gameName);
			
			if (type == null) {
				L.warn("Skipping directory \"" + gameName + "\"; not a valid game type");
				continue;
			}
			
			int count = 0;
			for (File zip : gameType.listFiles()) {
				String name = zip.getName();
				
				if (!name.endsWith(".zip")) {
					continue;
				}
				
				name = UtilString.removeLast(name, ".zip");
				if (mapRotation.put(type, name)) {
					count++;
				}
			}
			
			if (count == 0) {
				L.warn("No maps in rotation");
			} else {
				L.info("Found " + count + " map(s) for " + gameName);
			}
		}
	}
	
	public File getMapsDir(GameType gameType) {
		return new File(MAPS_PATH, gameType.getName());
	}
	
	public File getMapZip(GameType gameType, String map) {
		File dir = getMapsDir(gameType);
		return new File(dir, map + ".zip");
	}
	
	public SetMultimap<GameType, String> getMapRotation() {
		return mapRotation;
	}
	
	public void checkBounds() {
		for (Entry<World, DataMap> entry : data.entrySet()) {
			
			World world = entry.getKey();
			DataMap map = entry.getValue();
			
			for (Player player : world.getPlayers()) {
				Location loc = player.getLocation();
				
				double x = loc.getX();
				double y = loc.getY();
				double z = loc.getZ();
				
				if (x > map.maxX || x < map.minX || y < map.minY || y > map.maxY || z > map.maxZ || z < map.minZ) {
					EventPlayerOutOfBounds event = new EventPlayerOutOfBounds(player);
					UtilEvent.call(event);
				}
			}
			
		}
	}
	
	@EventHandler
	public void loadMap(WorldLoadEvent event) {
		loadData(event.getWorld());
	}
	
	@EventHandler
	public void unloadMap(WorldUnloadEvent event) {
		removeData(event.getWorld());
	}
	
	public void loadData(World world) {
		File file = new File(world.getWorldFolder(), DataMap.FILE_NAME);
		if (file.exists()) {
			DataMap map = new DataMap(world, file);
			
			try {
				map.loadFile();
			} catch (IOException ex) {
				L.error(ex, "Unable to load map data in world \"" + world.getName() + "\"");
				return;
			}
			
			addData(world, map);
		}
	}
	
	public void addData(World world, DataMap map) {
		data.put(world, map);
	}
	
	public void removeData(World world) {
		data.remove(world);
	}
	
	public DataMap getData(World world) {
		return data.get(world);
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
}
