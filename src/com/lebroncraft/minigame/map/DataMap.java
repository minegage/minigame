package com.lebroncraft.minigame.map;


import java.io.File;
import java.io.IOException;

import org.bukkit.World;

import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.log.L;


public class DataMap
		extends DataFile {
		
	public static final String FILE_NAME = "map data.txt";
	
	public String name = "null";
	public String author = "null";
	
	public int minX = Integer.MIN_VALUE;
	public int maxX = Integer.MAX_VALUE;
	
	public int minY = 0;
	public int maxY = 255;
	
	public int minZ = Integer.MIN_VALUE;
	public int maxZ = Integer.MAX_VALUE;
	
	public World world;
	
	public DataMap(World world, File file) {
		super(file);
		this.world = world;
	}
	
	@Override
	public void loadFile() throws IOException {
		super.loadFile();
		
		name = read("name");
		author = read("author");
		
		try {
			minX = toInt(read("minx"));
			maxX = toInt(read("maxx"));
			
			minZ = toInt(read("minz"));
			maxZ = toInt(read("maxz"));
			
			minY = toInt(read("miny"));
			maxY = toInt(read("maxy"));
		} catch (Exception ex) {
			L.warn("Unable to parse all bounds; Bad map data format in file \"" + file.getPath() + "\"");
		}
	}
	
	@Override
	public void saveFile() throws IOException {
		write("name", name);
		write("author", author);
		
		write("minx", minX);
		write("maxx", maxX);
		
		write("miny", minY);
		write("maxy", maxY);
		
		write("minz", minZ);
		write("maxz", maxZ);
		
		super.saveFile();
	}
	
}
