package com.lebroncraft.minigame.winnable;


import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;

import com.lebroncraft.minigame.stats.StatTracker;
import com.lebroncraft.minigame.team.GameTeam;


public class WinnableTeam
		extends Winnable<GameTeam> {
		
	public WinnableTeam(GameTeam team) {
		super(team);
	}
	
	@Override
	public Set<Player> getPlayers() {
		return new HashSet<>(winnable.getPlayers());
	}
	
	@Override
	public Integer getScore(StatTracker statTracker, String stat) {
		return statTracker.get(winnable, stat);
	}
	
	@Override
	public String getName() {
		return winnable.getName();
	}
	
	@Override
	public String getColour() {
		return winnable.getPrefix();
	}
	
}
