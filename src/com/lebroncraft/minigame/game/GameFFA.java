package com.lebroncraft.minigame.game;


import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.event.PlayerStateChangeEvent;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.stats.Stat;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.winnable.PlayerComparator;
import com.lebroncraft.minigame.winnable.Winnable;
import com.lebroncraft.minigame.winnable.WinnablePlayer;


public abstract class GameFFA
		extends Game {
		
	/* Index corresponds to place in leaderboard */
	private LinkedList<Player> places = new LinkedList<>();
	
	public GameFFA(MinigameManager manager, GameType type, String[] description, Kit... kits) {
		super(manager, type, description, kits);
		
		damagePlayerVsSelfTeam = true;
	}
	
	@Override
	public void createTeams() {
		createTeam("Players", C.cYellow);
	}
	
	@Override
	public void assignTeam(Player player) {
		getTeam().addPlayer(player);
	}
	
	@EventHandler
	public void setPlace(PlayerStateChangeEvent event) {
		// Null prev state means the player has just joined
		if (event.getPrevState() != null && event.getNewState() == PlayerState.OUT) {
			places.addFirst(event.getPlayer());
		}
	}
	
	@Override
	public void clearReference(Player player) {
		super.clearReference(player);
		places.remove(player);
	}
	
	@Override
	public boolean endCheck() {
		return getPlayers(PlayerState.IN).size() < 2;
	}
	
	@Override
	public void end() {
		super.end();
	}
	
	/* Common end checks */
	
	/**
	 * @param state
	 *        The {@link PlayerState} to check for
	 * @param players
	 *        The number of players with the specified {@link PlayerState} which should result in
	 *        the game ending
	 * @return If the game should end
	 */
	protected boolean endCheckState(PlayerState state, int players) {
		return getPlayers(state).size() <= players;
	}
	
	protected boolean endCheckTime(int ticks) {
		return getStateTicks() >= ticks;
	}
	
	public GameTeam getTeam() {
		return getTeams().get(0);
	}
	
	@Override
	protected List<Winnable<?>> getWinnerPlaces() {
		if (deathOut) {
			// There can only be one
			List<Player> alive = getPlayersIn();
			if (alive.size() > 1) {
				places.clear();
			} else if (alive.size() == 1) {
				places.addFirst(alive.get(0));
			}
			
			return places.stream()
					.map(pl -> new WinnablePlayer(pl))
					.collect(Collectors.toList());
		} else {
			List<Player> alive = getPlayersIn();
			alive.sort(getWinnerComparator());
			
			return alive.stream()
					.map(player -> new WinnablePlayer(player))
					.collect(Collectors.toList());
		}
	}
	
	protected Comparator<Player> getWinnerComparator() {
		return new PlayerComparator(this.stats, Stat.KILLS);
	}
	
}
