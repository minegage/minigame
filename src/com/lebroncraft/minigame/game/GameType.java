package com.lebroncraft.minigame.game;


import com.lebroncraft.minigame.game.games.bountyhunter.GameBountyHunter;
import com.lebroncraft.minigame.game.games.kitpvp.GameKitPVP;
import com.lebroncraft.minigame.game.games.oitq.GameOITQ;
import com.lebroncraft.minigame.game.games.paintball.GamePaintball;
import com.lebroncraft.minigame.game.games.riot.GameRiot;
import com.lebroncraft.minigame.game.games.skywars.GameSkywars;
import com.lebroncraft.minigame.game.games.spleef.GameSpleef;
import com.lebroncraft.minigame.game.games.survivalgames.GameSurvivalGames;
import com.lebroncraft.minigame.game.games.tntrun.GameTNTRun;
import com.lebroncraft.minigame.game.games.xpwars.GameXPWars;


public enum GameType {
	
	XPWARS("XP Wars", "XPW", GameXPWars.class),
	ONE_IN_THE_QUIVER("One in the Quiver", "OITQ", GameOITQ.class),
	SKYWARS("Skywars", "SKY", GameSkywars.class),
	SURVIVAL_GAMES("Survival Games", "SG", GameSurvivalGames.class),
	KIT_PVP("Kit PVP", "KIT", GameKitPVP.class),
	SPLEEF("Spleef", "SPL", GameSpleef.class),
	TNT_RUN("TNT Run", "RUN", GameTNTRun.class),
	BOUNTY_HUNTER("Bounty Hunter", "BH", GameBountyHunter.class),
	RIOT("Riot", "RIOT", GameRiot.class),
	PAINTBALL("Paintball", "PNT", GamePaintball.class),
	
	;
	private String nick;
	private String name;
	private Class<? extends Game> clazz;
	
	private GameType(String name, String nick, Class<? extends Game> clazz) {
		this.name = name;
		this.nick = nick;
		this.clazz = clazz;
	}
	
	public String getNick() {
		return nick;
	}
	
	public String getName() {
		return name;
	}
	
	public Class<? extends Game> getClazz() {
		return clazz;
	}
	
}
