package com.lebroncraft.minigame.game;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;

import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.team.GameTeamSizeComparator;
import com.lebroncraft.minigame.winnable.Winnable;
import com.lebroncraft.minigame.winnable.WinnableTeam;


public abstract class GameTDM
		extends Game {
		
	public GameTDM(MinigameManager manager, GameType type, String[] description, Kit... kits) {
		super(manager, type, description, kits);
		
		// Assume that kits will be defined by team
		if (kits.length == 0) {
			teamUniqueKits = true;
		}
		
	}
	
	private GameTeamSizeComparator comparator = new GameTeamSizeComparator();
	
	@Override
	public void assignTeam(Player player) {
		List<GameTeam> teams = getTeamsVisible();
		teams.sort(comparator);
		teams.get(0)
				.addPlayer(player);
	}
	
	@Override
	protected List<Winnable<?>> getWinnerPlaces() {
		// Cast from <GameTeam> to <?>
		return getWinners().stream()
				.map(team -> new WinnableTeam(team))
				.collect(Collectors.toList());
	}
	
	protected abstract List<GameTeam> getWinners();
	
	protected boolean endCheckSurvival() {
		int survivingTeams = 0;
		for (GameTeam team : getTeams()) {
			if (team.getPlayersIn()
					.size() > 0) {
				survivingTeams += 1;
			}
		}
		
		return survivingTeams <= 1;
	}
	
	protected List<GameTeam> getWinnersSurvival() {
		List<GameTeam> winners = new ArrayList<>();
		
		for (GameTeam team : getTeams()) {
			if (team.isAlive()) {
				winners.add(team);
			}
		}
		
		// Can't have 2 winners
		if (winners.size() > 1) {
			winners.clear();
		}
		
		return winners;
	}
	
}
