package com.lebroncraft.minigame.game;


import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lebroncraft.core.block.BlockManager;
import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.combat.DeathMessenger.DeathMessageMode;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.Ordinal;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilMath;
import com.lebroncraft.core.common.util.UtilPlayer;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilTime;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.common.util.UtilZip;
import com.lebroncraft.core.condition.VisibilityManager;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.spawn.SpawnManager;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.board.GameBoardManager;
import com.lebroncraft.minigame.game.event.GameStateChangeEvent;
import com.lebroncraft.minigame.game.event.PlayerStateChangeEvent;
import com.lebroncraft.minigame.item.ItemManager;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.map.MapManager;
import com.lebroncraft.minigame.stats.Stat;
import com.lebroncraft.minigame.stats.StatTracker;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.winnable.Winnable;


public abstract class Game
		extends PluginModule {
		
	public static String DATA_FILE = "game data.txt";
	
	protected GameManager gameManager;
	protected GameBoardManager boardManager;
	
	protected GameType type;
	protected String[] description;
	protected List<Kit> globalKits;
	public Kit defaultKit = null;
	
	public boolean teamUniqueKits = false;
	
	protected SafeMap<Player, PlayerState> states = new SafeMap<>();
	private List<GameTeam> teams = Lists.newArrayList();
	private SafeMap<Player, Kit> selectedKits = new SafeMap<>();
	protected StatTracker stats;
	
	public boolean paused = false;
	protected int totalTicks = 0;
	protected int stateTicks = 0;
	
	protected String mapName;
	
	public boolean spawnCommand = false;
	
	public boolean lobby = true;
	public boolean skippedWaiting = false;
	
	public GameMode defaultMode = GameMode.SURVIVAL;
	
	/* Players required to start the game */
	public int minPlayers = 2;
	/* Players required to play the game - game will end when player count is less than this */
	public int minPlayersAbsolute = 1; // TODO: Change to 2 for release
	/* When there are this many players, players without permissions will not be allowed to join */
	public int maxPlayers = 12;
	/* When there are this many players, player which are not admin will not be allowed to join */
	public int maxPlayersAbsolute = 20;
	
	public boolean joinMessageLive = true;
	public boolean quitMessageLive = true;
	
	/* Enviro is a damage source which isn't a player */
	public boolean damage = true;
	public boolean damageVsPlayer = true;
	public boolean damageVsEnviro = true;
	
	public boolean damagePlayerVsPlayer = true;
	public boolean damageEnviroVsPlayer = true;
	
	public boolean damageEnviroVsEnviro = true;
	public boolean damagePlayerVsEnviro = true;
	
	public boolean damageFallVsPlayer = false;
	public boolean damageFallVsEnviro = true;
	
	public boolean damagePlayerVsSelf = false;
	public boolean damagePlayerVsSelfTeam = false;
	
	public DeathMessageMode deathMessageMode = DeathMessageMode.ALL;
	
	public int invincibleRespawnTicks = 60;
	
	public boolean projectiles = true;
	public boolean removeArrows = true;
	
	public boolean timed = false;
	public double timeLimit = 60 * 3;
	
	public boolean hunger = false;
	public float spawnSaturation = 5.0F;
	public float spawnExhaustion = 0.0F;
	public int spawnHunger = 20;
	
	public boolean blockBreak = false;
	public Set<MaterialData> blockBreakAllow = Sets.newHashSet();
	public Set<MaterialData> blockBreakDeny = Sets.newHashSet();
	
	public boolean blockPlace = false;
	public Set<MaterialData> blockPlaceAllow = Sets.newHashSet();
	public Set<MaterialData> blockPlaceDeny = Sets.newHashSet();
	
	public boolean itemDropDeath = false;
	public boolean itemDrop = false;
	public boolean arrowStay = false;
	public Set<MaterialData> itemDropAllow = Sets.newHashSet();
	public Set<MaterialData> itemDropDeny = Sets.newHashSet();
	
	public boolean armourMove = false;
	public boolean itemMove = true;
	public Set<MaterialData> itemMoveAllow = Sets.newHashSet();
	public Set<MaterialData> itemMoveDeny = Sets.newHashSet();
	
	public boolean itemTake = false;
	public Set<MaterialData> itemTakeAllow = Sets.newHashSet();
	public Set<MaterialData> itemTakeDeny = Sets.newHashSet();
	
	public SafeMap<Location, Double> safeZones = new SafeMap<>();
	
	public boolean itemDamage = true;
	
	public boolean inAllowFlight = false;
	public boolean inFlight = false;
	public boolean boundaryDamage = true;
	
	public boolean deathSpecItem = true;
	public boolean deathOut = true;
	public int deathOutCount = 1;
	
	/* If true, sets player state to OUT when joining a game in progress */
	public boolean joinOut = true;
	
	protected SafeMap<Player, Integer> respawnTimes = new SafeMap<>();
	
	private GameState state = null;
	
	public boolean weather = false;
	
	public boolean explain = true;
	public boolean explainFreeze = true;
	public double explainTime = 10.0;
	public boolean explainBlockEdit = false;
	
	public boolean explaining = false;
	
	public Set<InventoryType> restrictedMenus = Sets.newHashSet();
	
	public Game(MinigameManager manager, GameType type, String[] description, Kit... kits) {
		super(type.getName(), manager);
		
		this.gameManager = manager.getGameManager();
		
		this.globalKits = Lists.newArrayList(kits);
		for (Kit kit : kits) {
			if (defaultKit == null) {
				defaultKit = kit;
			}
			
			kit.setGame(this);
		}
		
		if (defaultKit != null) {
			for (Player player : UtilServer.players()) {
				selectedKits.put(player, defaultKit);
			}
		}
		
		createTeams();
		createTeam("Spectator", C.cGray).setVisible(false);
		
		this.type = type;
		this.description = description;
		
		this.stats = new StatTracker(this, getStatTable(), getStats());
		
		for (Player player : UtilServer.players()) {
			setState(player, PlayerState.IN);
			giveSpectator(player, false);
		}
	}
	
	
	@Override
	protected void onDisable() {
		for (Player player : UtilServer.players()) {
			clearReference(player);
		}
		
		if (boardManager != null) {
			boardManager.disable();
		}
		stats.disable();
		
		// Unregister kits and attributes
		for (Kit kit : globalKits) {
			kit.dispose();
		}
		
		for (GameTeam team : getTeams()) {
			team.dispose();
		}
	}
	
	public abstract void loadData(DataFile data);
	
	public abstract boolean endCheck();
	
	public abstract void createTeams();
	
	/**
	 * Called when a player joins late, and {@link Game#joinOut} is false
	 */
	public abstract void assignTeam(Player player);
	
	protected abstract List<Winnable<?>> getWinnerPlaces();
	
	/**
	 * Loads all relevant information from the specified world. Assumes that the world file exists
	 * 
	 * @param mapName
	 *        The world name to load from the global world folder
	 * @return True if successful, false otherwise
	 */
	public void load(String mapName) throws IOException {
		if (mapName == null) {
			throw new NullPointerException("Map cannot be null");
		}
		
		this.mapName = mapName;
		
		MapManager mapManager = gameManager.getMapManager();
		
		File zip = mapManager.getMapZip(type, mapName);
		File worldFile = new File(Bukkit.getWorldContainer(), mapName);
		
		UtilZip.extract(zip, worldFile);
		
		World world = UtilWorld.load(mapName);
		File worldDir = world.getWorldFolder();
		
		File gameFile = new File(worldDir, DATA_FILE);
		DataFile data = new DataFile(gameFile);
		
		data.loadFile();
		
		for (Entry<String, String> entry : data.getEntries("spawns")) {
			String desc = entry.getKey();
			String info = entry.getValue();
			
			List<String> descs = data.desc(desc);
			descs.remove("spawns");
			
			String teamName = descs.get(0);
			GameTeam team = getTeam(teamName);
			
			if (team == null) {
				L.severe("Undefined team \"" + teamName + "\" in spawn list of map \"" + mapName + "\"; skipping");
				return;
			}
			
			List<Location> spawns = data.toLocations(info, world);
			team.getSpawns()
					.addAll(spawns);
		}
		
		loadData(data);
	}
	
	@EventHandler
	public final void gameTick(TickEvent event) {
		if (event.getTick() == Tick.TICK_1) {
			
			totalTicks += 1;
			if (!paused) {
				stateTicks += 1;
			}
			
			Iterator<Entry<Player, Integer>> respawnIt = respawnTimes.entrySet()
					.iterator();
					
			while (respawnIt.hasNext()) {
				Entry<Player, Integer> entry = respawnIt.next();
				
				Player player = entry.getKey();
				Integer respawnTicks = entry.getValue();
				
				int timeLeft = UtilTime.timeLeft(respawnTicks);
				
				if (timeLeft <= 0) {
					respawn(player);
					respawnIt.remove();
				} else {
					double respawnSeconds = UtilTime.toSeconds(timeLeft);
					sendRespawnTitle(player, respawnSeconds);
				}
			}
		}
	}
	
	public void sendRespawnTitle(Player player, double respawnSeconds) {
		if (isPlaying()) {
			respawnSeconds = UtilMath.round(respawnSeconds, 1);
			UtilUI.sendSubtitle(player, C.cGreen + "Respawning in " + respawnSeconds + "s...");
		}
	}
	
	public void clearReference(Player player) {
		for (GameTeam team : teams) {
			team.removePlayer(player);
		}
		
		selectedKits.remove(player);
		states.remove(player);
		respawnTimes.remove(player);
	}
	
	/**
	 * Called when state is changed to {@link GameState#PLAYING}
	 */
	public void start() {
		boardManager = new GameBoardManager(this);
		
		if (explain) {
			explaining = true;
			
			for (Player player : getPlayersIn()) {
				C.pRaw(player, "");
				C.pRaw(player, "");
				C.pGeneral(player, C.cBold + "Game", C.cGreen + C.cBold + getName());
				C.pRaw(player, "");
				for (String str : description) {
					C.pRaw(player, C.t1 + str);
				}
				C.pRaw(player, "");
			}
		}
		
		setDeathMessageMode(this.deathMessageMode);
		stats.setRecording(true);
	}
	
	public void end() {
		setDeathMessageMode(DeathMessageMode.NONE);
		
		List<Winnable<?>> winners = getWinnerPlaces();
		
		String title;
		String subtitle;
		if (winners == null || winners.size() == 0) {
			title = "Nobody won!";
			subtitle = "";
		} else {
			
			Winnable<?> winner = winners.get(0);
			
			String colour = winner.getColour();
			
			title = colour + winner.getName();
			subtitle = colour + " won the game!";
			
			if (winners.size() > 1) {
				List<String> colours = C.rainbow(C.cGreenD);
				Collections.reverse(colours);
				
				int numWinners = Math.min(3, winners.size());
				for (int i = 0; i < numWinners; i++) {
					String name = winners.get(i)
							.getName();
					String placeColour = UtilJava.getWrappedIndex(colours, i) + C.cBold;
					String placeWord = Ordinal.shortForm(i + 1);
					
					String outPrefix = placeColour + placeWord;
					String outName = placeColour + name;
					
					String out = C.t1 + outPrefix + C.fDash() + outName;
					
					C.bRaw(out);
				}
			}
		}
		
		UtilServer.players()
				.forEach(player -> UtilUI.sendTitles(player, title, subtitle, 5, 100, 10));
				
		if (winners != null) {
			recordWinLossStats(winners.stream()
					.findFirst()
					.orElse(null));
		}
		
		stats.setRecording(false);
	}
	
	private void recordWinLossStats(Winnable<?> winner) {
		if (winner == null) {
			return;
		}
		
		Set<Player> winners = winner.getPlayers();
		
		if (winners == null) {
			return;
		}
		
		for (Player player : winners) {
			stats.increment(player, Stat.WINS, 1);
		}
		
		Set<Player> losers = UtilServer.playersSet();
		losers.removeAll(winners);
		
		for (Player player : losers) {
			stats.increment(player, Stat.LOSSES, 1);
		}
	}
	
	/**
	 * Resets the inventory, hunger, potion effects, fly/walk speed, visibility, etc. Teleports the
	 * player to respawn point
	 */
	public void respawn(Player player) {
		if (inLobby()) {
			player.teleport(UtilWorld.getMainWorld()
					.getSpawnLocation());
			return;
		}
		
		Location respawnLoc = null;
		GameTeam team = getTeam(player);
		if (team == null) {
			respawnLoc = getSpecSpawn();
		} else {
			respawnLoc = team.nextSpawn();
		}
		
		if (respawnLoc == null) {
			respawnLoc = player.getWorld()
					.getSpawnLocation();
		}
		
		// Vanish before teleporting to avoid weird teleport/flying animation
		VisibilityManager vis = gameManager.getMinigame()
				.getVisibilityManager();
		vis.setVanished(player, true);
		
		player.teleport(respawnLoc);
		
		if (getState(player) == PlayerState.IN) {
			giveSpectator(player, false);
			
			Kit kit = getKit(player);
			if (kit != null) {
				kit.equip(player);
			}
			
			player.setNoDamageTicks(invincibleRespawnTicks);
		}
		
		UtilUI.clearTitle(player);
	}
	
	public void respawn(Player player, double respawnSeconds) {
		giveDeathEffects(player);
		
		int ticks = UtilTime.toTicks(respawnSeconds);
		int time = UtilServer.currentTick() + ticks;
		respawnTimes.put(player, time);
		
		int titleTime = Math.min(ticks - 10, 50);
		
		UtilUI.sendTitle(player, C.cRed + "You died!", 0, titleTime, 10);
		sendRespawnTitle(player, respawnSeconds);
	}
	
	private PotionEffect respawnBlind = new PotionEffect(PotionEffectType.BLINDNESS, 30, 0, false, false);
	private PotionEffect respawnSlow = new PotionEffect(PotionEffectType.SLOW, 20, 0, false, false);
	
	public void giveDeathEffects(Player player) {
		giveSpectator(player, true);
		
		if (deathSpecItem) {
			player.getInventory()
					.setItem(4, ItemManager.specItem);
		}
		
		player.setVelocity(new Vector(0, 0.5, 0));
		
		respawnBlind.apply(player);
		respawnSlow.apply(player);
	}
	
	public void out(Player player, boolean death) {
		if (death) {
			giveDeathEffects(player);
			UtilUI.sendTitles(player, C.cRed + "You died!", C.cGold + C.cBold + "You will respawn next game!", 0, 60, 10);
		} else {
			GameTeam specTeam = getSpectatorTeam();
			specTeam.addPlayer(player);
			
			giveSpectator(player, true);
		}
		
		player.getInventory()
				.setItem(8, ItemManager.lobbyItem);
				
		setState(player, PlayerState.OUT);
	}
	
	public void giveSpectator(Player player, boolean spectating) {
		UtilPlayer.reset(player);
		
		if (spectating) {
			player.setAllowFlight(true);
			player.setFlying(true);
			
			// Remove the player as a target to creatures
			for (Entity entity : player.getWorld()
					.getEntities()) {
					
				if (entity instanceof Creature) {
					Creature creature = (Creature) entity;
					
					if (creature.getTarget() != null && creature.getTarget()
							.equals(player)) {
						creature.setTarget(null);
					}
				}
			}
			
			
		} else {
			player.setAllowFlight(inAllowFlight);
			player.setFlying(inFlight);
		}
		UtilPlayer.setCollides(player, !spectating);
		
		gameManager.getMinigame()
				.getVisibilityManager()
				.setVanished(player, spectating);
	}
	
	public void setRecordStats(boolean recordStats) {
		stats.setRecording(recordStats);
	}
	
	public StatTracker getStatTracker() {
		return stats;
	}
	
	public void setRestricted(InventoryType menu, boolean restricted) {
		if (restricted) {
			restrictedMenus.add(menu);
		} else {
			restrictedMenus.remove(menu);
		}
	}
	
	public List<Player> getPlayers(PlayerState state) {
		return UtilServer.players()
				.stream()
				.filter(player -> getState(player) == state)
				.collect(Collectors.toList());
	}
	
	public List<Player> getPlayersIn() {
		return getPlayers(PlayerState.IN);
	}
	
	public List<Player> getPlayersOut() {
		return getPlayers(PlayerState.OUT);
	}
	
	public List<Player> getPlayersSpectating() {
		return UtilServer.players()
				.stream()
				.filter(player -> isSpectating(player))
				.collect(Collectors.toList());
	}
	
	public List<Player> getPlayersNotSpectating() {
		return UtilServer.players()
				.stream()
				.filter(player -> !isSpectating(player))
				.collect(Collectors.toList());
	}
	
	public String[] getStats() {
		return new String[] { Stat.WINS, Stat.LOSSES, Stat.EARNED };
	}
	
	public List<GameTeam> getTeams() {
		return teams;
	}
	
	public List<GameTeam> getTeamsVisible() {
		return getTeams().stream()
				.filter(team -> team.isVisible())
				.collect(Collectors.toList());
	}
	
	public GameTeam getSpectatorTeam() {
		return getTeam("Spectator");
	}
	
	public GameTeam createTeam(String name, String prefix, Kit... kits) {
		GameTeam team = new GameTeam(this, name, prefix, kits);
		teams.add(team);
		return team;
	}
	
	/**
	 * @return The GameTeam with the specified name. Case sensitive.
	 */
	public GameTeam getTeamExact(String name) {
		return teams.stream()
				.filter(team -> team.getName()
						.equals(name))
				.findFirst()
				.orElse(null);
	}
	
	/**
	 * @return The GameTeam with the specified name. Case insensitive.
	 */
	public GameTeam getTeam(String name) {
		return teams.stream()
				.filter(team -> team.getName()
						.equalsIgnoreCase(name))
				.findFirst()
				.orElse(null);
	}
	
	public GameTeam getTeam(Player player) {
		return teams.stream()
				.filter(team -> team.getPlayers()
						.contains(player))
				.findFirst()
				.orElse(null);
	}
	
	/**
	 * @return Tick value since the game was initialized
	 */
	public int getTicks() {
		return totalTicks;
	}
	
	/**
	 * @return Tick value since the last GameState change
	 */
	public int getStateTicks() {
		return stateTicks;
	}
	
	public double getStateSeconds() {
		return UtilTime.toSeconds(getStateTicks());
	}
	
	public double getRemainingSeconds() {
		double playTime = timeLimit;
		
		if (explain) {
			if (explaining) {
				playTime -= explainTime;
			}
		}
		
		return playTime - getStateSeconds();
	}
	
	public double getStateMinutes() {
		return 60.0 * getStateSeconds();
	}
	
	public void setTicks(int ticks) {
		this.totalTicks = ticks;
	}
	
	public void setStateTicks(int stateTicks) {
		this.stateTicks = stateTicks;
	}
	
	/**
	 * @return The respawn time of the player, in ticks. If the player is not respawning, -1 is
	 *         returned.
	 */
	public int getRespawnTime(Player player) {
		return respawnTimes.getOrDefault(player, -1);
	}
	
	public boolean isRespawning(Player player) {
		return respawnTimes.containsKey(player);
	}
	
	public boolean isSpectating(Player player) {
		return getState(player) == PlayerState.OUT || isRespawning(player);
	}
	
	public boolean canInteract(Player player) {
		if (explaining && explainFreeze) {
			return false;
		}
		
		return !isSpectating(player);
	}
	
	public Location getSpecSpawn() {
		return SpawnManager.instance.getSpawnpoint(getMap());
	}
	
	public List<Kit> getGlobalKits() {
		return globalKits;
	}
	
	public Kit getKit(String name) {
		if (teamUniqueKits) {
			for (GameTeam team : getTeams()) {
				Kit kit = team.getKit(name);
				if (kit != null) {
					return kit;
				}
			}
			
			return null;
		} else {
			return globalKits.stream()
					.filter(kit -> kit.getName()
							.equals(name))
					.findFirst()
					.orElse(null);
		}
	}
	
	public Kit getKit(Player player) {
		if (teamUniqueKits) {
			GameTeam team = getTeam(player);
			if (team == null) {
				return null;
			}
			
			return team.getKit(player);
		} else {
			return selectedKits.get(player);
		}
	}
	
	public void setKit(Player player, Kit kit) {
		if (teamUniqueKits) {
			
			for (GameTeam team : getTeams()) {
				if (team.getKits()
						.contains(kit)) {
					team.setKit(player, kit);
					return;
				}
			}
			
			throw new NullPointerException("Can't find host of kit " + kit.getName());
			
		} else {
			selectedKits.put(player, kit);
		}
	}
	
	public void setState(GameState state) {
		GameState oldState = this.state;
		this.state = state;
		this.stateTicks = 0;
		
		GameStateChangeEvent event = new GameStateChangeEvent(oldState, state);
		UtilEvent.call(event);
	}
	
	public GameState getState() {
		return state;
	}
	
	public boolean inLobby() {
		GameState state = getState();
		return state == GameState.LOADING || state == GameState.WAITING || state == GameState.STARTING;
	}
	
	public boolean isPlaying() {
		return getState() == GameState.PLAYING;
	}
	
	public void setDeathMessageMode(DeathMessageMode mode) {
		gameManager.getMinigameManager()
				.getCombatManager()
				.getDeathMessenger().mode = mode;
	}
	
	public SafeMap<Player, PlayerState> getPlayerStates() {
		return states;
	}
	
	public PlayerState getState(Player player) {
		return getPlayerStates().get(player);
	}
	
	public void setState(Player player, PlayerState state) {
		PlayerState prevState = states.get(player);
		getPlayerStates().put(player, state);
		
		PlayerStateChangeEvent event = new PlayerStateChangeEvent(player, prevState, state);
		UtilEvent.call(event);
	}
	
	public void addSafezone(Location location, double radius) {
		safeZones.put(location, radius);
	}
	
	public double removeSafezone(Location location) {
		return safeZones.remove(location);
	}
	
	public GameType getType() {
		return type;
	}
	
	@Override
	public String getName() {
		return getType().getName();
	}
	
	public BlockManager getFallingBlockManager() {
		return gameManager.getFallingBlockManager();
	}
	
	public World getMap() {
		return Bukkit.getWorld(mapName);
	}
	
	public GameBoardManager getBoardManager() {
		return boardManager;
	}
	
	/**
	 * Method which can be overridden to edit a Board after giving it to them
	 * 
	 * @param player
	 * @param board
	 *        The board given to the player.
	 */
	public void giveBoard(Player player, Board board) {
	
	}
	
	public static enum GameState {
		LOADING,
		WAITING,
		STARTING,
		PLAYING,
		ENDING,
		DEAD;
	}
	
	public static enum PlayerState {
		IN,
		OUT;
	}
	
	public String getStatTable() {
		return getName().replaceAll(" ", "_");
	}
	
}
