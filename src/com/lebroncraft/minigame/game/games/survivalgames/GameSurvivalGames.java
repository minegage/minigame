package com.lebroncraft.minigame.game.games.survivalgames;


import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameFFA;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.kit.Kit;


public class GameSurvivalGames
		extends GameFFA {
		
	protected GameSurvivalGames(MinigameManager manager) {
		super(manager, GameType.SURVIVAL_GAMES, new String[] { }, new Kit[] { });
	}
	
	@Override
	public void loadData(DataFile data) {}
	
	@Override
	public boolean endCheck() {
		return false;
	}
	
}
