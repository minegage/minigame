package com.lebroncraft.minigame.game.games.kitpvp.shop;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.menu.button.Button;


public class ShopButton
		extends Button {
		
	private ShopMenu menu;
	private KitShopItem item;
	
	public ShopButton(ShopMenu menu, KitShopItem item) {
		this.menu = menu;
		this.item = item;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		return menu.attemptBuy(player, item);
	}
	
}
