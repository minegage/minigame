package com.lebroncraft.minigame.game.games.kitpvp.kits;


import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.core.common.build.ItemBuild;
import com.lebroncraft.core.common.util.UtilArmour;
import com.lebroncraft.core.common.util.UtilArmour.ArmourType;
import com.lebroncraft.minigame.game.games.kitpvp.KitPvpBase;


public class KitRanger
		extends KitPvpBase {
		
	public KitRanger() {
		super("Ranger", new String[0]);
		
		addArmourUpgrades(10, 15, 20);
		addSwordUpgrades(Material.IRON_SWORD, 10, 15, 25);
		
		ItemStack sword = ItemBuild.create(Material.IRON_SWORD)
				.enchant(Enchantment.DAMAGE_ALL, 2)
				.item();
				
		setMobItems(UtilArmour.getArmourSet(ArmourType.GOLD, sword));
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		super.giveItems(inv);
		
		giveMobItems(inv);
		
		ItemStack bow = ItemBuild.create(Material.BOW)
				.enchant(Enchantment.ARROW_INFINITE)
				.enchant(Enchantment.ARROW_DAMAGE, 2)
				.item();
				
		ItemStack arrow = new ItemStack(Material.ARROW);
		
		inv.setItem(1, bow);
		inv.setItem(28, arrow);
	}
	
}
