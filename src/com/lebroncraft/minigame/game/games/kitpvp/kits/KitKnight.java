package com.lebroncraft.minigame.game.games.kitpvp.kits;


import org.bukkit.Material;
import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.core.common.util.UtilArmour;
import com.lebroncraft.core.common.util.UtilArmour.ArmourType;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.minigame.game.games.kitpvp.KitPvpBase;


public class KitKnight
		extends KitPvpBase {
		
	public KitKnight() {
		super("Knight", new String[0]);
		this.mobType = MobType.ZOMBIE;
		
		setMobItems(UtilArmour.getArmourSet(ArmourType.IRON, UtilItem.create(Material.DIAMOND_SWORD)));
		addArmourUpgrades(25);
		addSwordUpgrades(Material.DIAMOND_SWORD, 20, 30);
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		super.giveItems(inv);
		giveMobItems(inv);
	}
	
}
