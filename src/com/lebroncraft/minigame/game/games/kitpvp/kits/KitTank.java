package com.lebroncraft.minigame.game.games.kitpvp.kits;


import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.minigame.game.games.kitpvp.KitPvpBase;
import com.lebroncraft.minigame.kit.attrib.AttribEffect;


public class KitTank
		extends KitPvpBase {
		
	public KitTank() {
		super("Tank", new String[0]);
		
		addAttribute(new AttribEffect("Slowness", PotionEffectType.SLOW.createEffect(Integer.MAX_VALUE, 0)));
		
		ItemStack sword = UtilItem.create(Material.IRON_AXE);
		mobHand = sword;
		
		mobHelm = new ItemStack(Material.DIAMOND_HELMET);
		mobChest = new ItemStack(Material.DIAMOND_CHESTPLATE);
		mobLegs = new ItemStack(Material.IRON_LEGGINGS);
		mobBoots = new ItemStack(Material.IRON_BOOTS);
		
		addSwordUpgrades(sword.getType(), 15, 20);
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		super.giveItems(inv);
		giveMobItems(inv);
	}
	
}
