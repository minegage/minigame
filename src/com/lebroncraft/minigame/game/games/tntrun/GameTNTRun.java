package com.lebroncraft.minigame.game.games.tntrun;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.material.MaterialData;

import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.util.UtilBlock;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilMat;
import com.lebroncraft.core.common.util.UtilTime;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameFFA;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.game.games.tntrun.kits.KitDefault;


public class GameTNTRun
		extends GameFFA {
		
	private Set<DecayBlock> decaying = new HashSet<>();
	
	public GameTNTRun(MinigameManager manager) {
		super(manager, GameType.TNT_RUN, new String[] { }, new KitDefault());
		
		this.damagePlayerVsPlayer = false;
		this.explainFreeze = false;
	}
	
	@Override
	public void loadData(DataFile data) {
	
	}
	
	private boolean isDecaying(Block block) {
		for (DecayBlock decay : decaying) {
			if (decay.block.equals(block)) {
				return true;
			}
		}
		
		return false;
	}
	
	@EventHandler
	public void tickDecay(TickEvent event) {
		if (event.isNot(Tick.TICK_1)) {
			return;
		}
		
		if (!isPlaying() || explaining) {
			return;
		}
		
		for (Player player : getPlayersIn()) {
			for (Block block : UtilEntity.getSupportingBlocks(player)) {
				if (!isDecaying(block)) {
					decaying.add(new DecayBlock(block));
				}
			}
		}
		
		Iterator<DecayBlock> decayIt = decaying.iterator();
		
		while (decayIt.hasNext()) {
			DecayBlock decay = decayIt.next();
			
			decay.tick();
			if (decay.isExpired()) {
				decayIt.remove();
			}
			
		}
	}
	
	private class DecayBlock {
		
		DecayState state = DecayState.STABLE;
		long decayStart = System.currentTimeMillis();
		Block block;
		
		public DecayBlock(Block block) {
			this.block = block;
		}
		
		public void tick() {
			long timePassed = UtilTime.timePassedSince(decayStart);
			DecayState newState = DecayState.getState(timePassed);
			
			if (state != newState) {
				
				if (newState == DecayState.FALL) {
					getFallingBlockManager().createFallingBlock(block, true);
				} else {
					UtilBlock.set(block, newState.data);
				}
			}
		}
		
		public boolean isExpired() {
			return UtilTime.hasPassedSince(decayStart, DecayState.FALL.time + 100L);
		}
		
	}
	
	private static enum DecayState {
		FALL(UtilMat.getData(Material.STAINED_CLAY, (byte) 14), 750L),
		RED(UtilMat.getData(Material.STAINED_CLAY, (byte) 14), 500L),
		ORANGE(UtilMat.getData(Material.STAINED_CLAY, (byte) 1), 250L),
		YELLOW(UtilMat.getData(Material.STAINED_CLAY, (byte) 4), 0L),
		STABLE(null, 0L),
		
		;
		private MaterialData data;
		private long time;
		
		private DecayState(MaterialData data, long time) {
			this.data = data;
			this.time = time;
		}
		
		public static DecayState getState(long timePassed) {
			for (DecayState state : values()) {
				if (UtilTime.hasPassed(timePassed, state.time)) {
					return state;
				}
			}
			
			return null;
		}
		
	}
	
}
