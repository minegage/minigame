package com.lebroncraft.minigame.game.games.tntrun.kits;


import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.minigame.kit.Kit;


public class KitDefault
		extends Kit {
		
	public KitDefault() {
		super("Default", new String[] { "The default TNT Run kit" });
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
	
	}
	
}
