package com.lebroncraft.minigame.game.games.oitq.kits;


import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.kit.attrib.Attrib;


public abstract class OITQKitBase
		extends Kit {
		
	protected ItemStack bow = new ItemStack(Material.BOW);
	protected ItemStack arrow = new ItemStack(Material.ARROW);
	
	public OITQKitBase(String name, String[] desc, Attrib... attributes) {
		super(name, desc, attributes);
		
		this.mobType = MobType.SKELETON;
	}
	
}
