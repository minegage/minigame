package com.lebroncraft.minigame.game.games.riot;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.objective.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.build.ItemBuild;
import com.lebroncraft.core.common.util.Rand;
import com.lebroncraft.core.common.util.UtilBlock;
import com.lebroncraft.core.common.util.UtilEffect;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilMat;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.event.CustomDeathEvent;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.core.timer.Timer;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameTDM;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.game.games.riot.kit.KitCop;
import com.lebroncraft.minigame.game.games.riot.kit.KitFirefighter;
import com.lebroncraft.minigame.game.games.riot.kit.KitRioter;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.team.JoinTeamEvent;
import com.lebroncraft.minigame.winnable.Winnable;
import com.lebroncraft.minigame.winnable.WinnableTeam;


public class GameRiot
		extends GameTDM {
		
	private Set<MaterialData> unbreakable = new HashSet<>();
	
	private final String POINTS_KEY = "Riot Points";
	
	public final Set<ItemStack> loot = new HashSet<>();
	public final Set<ItemStack> goodLoot = new HashSet<>();
	
	// The chaos required for the rioters to win
	private int chaosPerRioter;
	private int chaos = 100;
	
	private final int DEFAULT_CHAOS_PER_RIOTER = 465;
	
	public GameRiot(MinigameManager manager) {
		super(manager, GameType.RIOT, new String[] { C.cRed + "Rioters: " + C.cWhite + "Break things! Set buildings on fire! Blow up your",
				"neighbor's house! ...But watch out for cops!", "", C.cBlue + "Cops: " + C.cWhite
						+ "Stop the riot at all costs! Wait until backup arrives!" });
						
		maxPlayers = 16;
		
		deathOut = false;
		
		blockBreak = true;
		blockPlace = true;
		
		armourMove = false;
		
		damageFallVsPlayer = true;
		itemTake = false;
		itemDrop = false;
		
		this.timed = true;
		this.timeLimit = 60 * 3;
		
		ItemBuild tnt = ItemBuild.create(Material.TNT);
		ItemBuild snowball = ItemBuild.create(Material.SNOW_BALL);
		ItemBuild arrow = ItemBuild.create(Material.ARROW);
		
		Potion regen = new Potion(PotionType.REGEN);
		regen.setSplash(true);
		
		goodLoot(tnt.amount(2)
				.item());
		goodLoot(tnt.amount(3)
				.item());
		goodLoot(Material.FLINT_AND_STEEL);
		goodLoot(Material.FIREBALL); // Fire charge
		goodLoot(Material.STONE_SWORD);
		goodLoot(Material.STONE_AXE);
		goodLoot(Material.BOW);
		goodLoot(arrow.amount(4)
				.item());
		goodLoot(ItemBuild.create(Material.POTION)
				.potion(regen)
				.item());
				
		loot(tnt.amount(1)
				.item());
		loot(snowball.amount(12)
				.item());
		loot(snowball.amount(6)
				.item());
		loot(Material.WOOD_AXE);
		loot(new ItemStack(Material.WOOD, 8));
		
		unbreakable(Material.BEDROCK);
		unbreakable(Material.STONE);
		unbreakable(Material.STONE, 6);
		unbreakable(Material.DIRT);
		unbreakable(Material.GRASS);
		unbreakable(Material.BRICK);
		unbreakable(Material.COAL_BLOCK);
		unbreakable(Material.STEP);
		unbreakable(Material.WOOL, 0);
		unbreakable(Material.WOOL, 11);
		unbreakable(Material.STEP, 1);
		unbreakable(Material.STEP, 3);
	}
	
	private void unbreakable(Material mat, int data) {
		unbreakable.add(UtilMat.getData(mat, (byte) data));
	}
	
	private void unbreakable(Material mat) {
		unbreakable(mat, 0);
	}
	
	private void loot(Material material) {
		loot.add(new ItemStack(material));
	}
	
	private void loot(ItemStack item) {
		loot.add(item);
	}
	
	private void goodLoot(Material material) {
		goodLoot.add(new ItemStack(material));
	}
	
	private void goodLoot(ItemStack item) {
		goodLoot.add(item);
	}
	
	/**
	 * @return If cops are winning, this will be positive. If rioters are winning, this will be
	 *         negative. The size of the value is the amount of difference between the % completed
	 *         objective of the two teams
	 * 		
	 */
	public double getBalanceDifference() {
		double percentChaos = getPercentageChaos();
		double percentBackup = getPercentageBackup();
		
		return percentBackup - percentChaos;
	}
	
	public boolean isRiotBoosted() {
		// If the rioters are losing by 10%
		return getBalanceDifference() > 10;
	}
	
	public ItemStack getRandLoot() {
		Set<ItemStack> loot = this.loot;
		
		double goodLootChance = 20;
		if (isRiotBoosted()) {
			goodLootChance = 40;
		}
		
		if (Rand.rand(goodLootChance)) {
			loot = this.goodLoot;
		}
		
		return UtilJava.getRandIndex(loot);
	}
	
	@Override
	public void loadData(DataFile data) {
		String chaosString = data.read("chaos");
		if (chaosString != null) {
			this.chaosPerRioter = data.toInt(chaosString);
		} else {
			this.chaosPerRioter = DEFAULT_CHAOS_PER_RIOTER;
		}
		
		String backupString = data.read("backup");
		if (backupString != null) {
			this.timeLimit = data.toInt(backupString);
		} else {
			this.timeLimit = 210;
		}
	}
	
	@Override
	public boolean endCheck() {
		if (getBackupTime() <= 0) {
			C.bRaw("");
			C.bRaw(C.cBlue + C.cBold + "Backup has arrived! The cops win!");
			C.bRaw("");
			
			riotersWin = false;
			return true;
		}
		
		if (getRioterPoints() > chaos) {
			C.bRaw("");
			C.bRaw(C.cRed + C.cBold + "The cops couldn't stop the riot! The rioters win!");
			C.bRaw("");
			
			riotersWin = true;
			return true;
		}
		
		return false;
	}
	
	@EventHandler
	public void setChaos(JoinTeamEvent event) {
		if (event.getTeam()
				.equals(getRioters())) {
			this.chaos = chaosPerRioter * event.getTeam()
					.getPlayers()
					.size();
		}
	}
	
	@Override
	protected List<Winnable<?>> getWinnerPlaces() {
		List<Winnable<?>> winner = new ArrayList<>();
		GameTeam winnerTeam = null;
		if (riotersWin) {
			winnerTeam = getRioters();
		} else {
			winnerTeam = getCops();
		}
		
		winner.add(new WinnableTeam(winnerTeam));
		return winner;
	}
	
	@Override
	public void createTeams() {
		createTeam("Rioters", C.cRed, new KitRioter()).respawnSeconds = 10.0;
		createTeam("Cops", C.cBlue, new KitCop(), new KitFirefighter());
	}
	
	@Override
	public void assignTeam(Player player) {
		int cops = getCops().getPlayers()
				.size();
		int rioters = getRioters().getPlayers()
				.size();
				
		rioters = (int) Math.ceil(rioters / 3.0);
		
		if (cops > rioters) {
			getRioters().addPlayer(player);
		} else {
			getCops().addPlayer(player);
		}
	}
	
	private int chaosLabel = 0;
	private int chaosRow = 0;
	
	private int backupLabel = 0;
	private int backupRow = 0;
	
	@Override
	public void giveBoard(Player player, Board board) {
		ObjectiveSide side = board.getSideObjective();
		side.addRow("");
		chaosLabel = side.addRow(C.cGold + C.cBold + "Chaos");
		chaosRow = side.addRow(C.cGold + C.cBold + "0%");
		side.addRow("");
		backupLabel = side.addRow(C.cAqua + C.cBold + "Backup arrives");
		backupRow = side.addRow(C.cAqua + C.cBold + UtilUI.getTimer((int) timeLimit));
		side.addRow("");
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if (!isPlaying()) {
			return;
		}
		
		event.getPlayer()
				.setExp((float) getPercentageChaos() / 100F);
	}
	
	@EventHandler
	public void tickProgress(TickEvent event) {
		if (event.isNot(Tick.SEC_1) || !isPlaying() || explaining) {
			return;
		}
		
		if (chaosFlash) {
			chaosToggle = !chaosToggle;
		}
		
		if (backupFlash) {
			backupToggle = !backupToggle;
		}
		
		updateProgress();
	}
	
	private boolean chaosFlash = false;
	private boolean chaosToggle = false;
	
	private boolean backupFlash = false;
	private boolean backupToggle = false;
	
	private DecimalFormat format = new DecimalFormat("#");
	
	private void updateProgress() {
		String chaosColour = C.cGold + C.cBold;
		if (chaosToggle) {
			chaosColour = C.cRed + C.cBold;
		}
		String backupColour = C.cAqua + C.cBold;
		if (backupToggle) {
			backupColour = C.cBlue + C.cBold;
		}
		
		int chaosPoints = getRioterPoints();
		double percentage = (double) chaosPoints / (double) chaos * 100;
		if (percentage > 80.0) {
			chaosFlash = true;
		} else if (percentage > 100.0) {
			percentage = 100.0;
		}
		
		String chaosLabelMessage = chaosColour + "Chaos";
		String chaosValueMessage = chaosColour + format.format(percentage) + "%";
		
		double backupTime = getRemainingSeconds();
		if (backupTime < 45.0) {
			backupFlash = true;
		}
		String backupTimeDisplay = UtilUI.getTimer((int) backupTime);
		
		String backupLabelMessage = backupColour + "Backup arrives";
		String backupValueMessage = backupColour + backupTimeDisplay;
		
		for (Board board : boardManager.getPlayerBoards()) {
			ObjectiveSide side = board.getSideObjective();
			side.updateRow(chaosLabel, chaosLabelMessage);
			side.updateRow(chaosRow, chaosValueMessage);
			side.updateRow(backupLabel, backupLabelMessage);
			side.updateRow(backupRow, backupValueMessage);
		}
	}
	
	public GameTeam getCops() {
		return getTeam("Cops");
	}
	
	public GameTeam getRioters() {
		return getTeam("Rioters");
	}
	
	/* Point management */
	
	private int breakBuffer = 0;
	private int explodeBuffer = 0;
	private int fireBuffer = 0;
	
	private boolean riotersWin = false;
	
	public void addRioterPoints(int points) {
		if (!isPlaying()) {
			return;
		}
		
		// Multiply the points to get a higher precision while still using integers
		points *= 100;
		
		
		// Don't multiply punishment points
		if (points > 0) {
			double pointMultiplier = 1 + ( ( getBalanceDifference() ) / 100 );
			int multipliedPoints = (int) ( points * pointMultiplier );
			
			for (Player player : UtilServer.players()) {
				if (player.isOp()) {
					UtilUI.sendActionBar(player, C.cBold + points + " -> " + multipliedPoints);
				}
			}
			
			points = multipliedPoints;
		}
		
		stats.increment(getRioters(), POINTS_KEY, points);
		
		float exp = (float) getPercentageChaos() / 100F;
		for (Player player : UtilServer.players()) {
			player.setExp(exp);
		}
	}
	
	public int getRioterPoints() {
		return stats.get(getRioters(), POINTS_KEY) / 100;
	}
	
	public double getBackupTime() {
		double playTime = getStateSeconds() - explainTime;
		return timeLimit - playTime;
	}
	
	public double getPercentageChaos() {
		return (double) getRioterPoints() / (double) chaos * 100.0;
	}
	
	public double getPercentageBackup() {
		double totalTime = timeLimit - explainTime;
		double playTime = getStateSeconds() - explainTime;
		
		return playTime / totalTime * 100.0;
	}
	
	/* Events */
	
	@EventHandler
	public void cancelDefaultBreak(BlockBreakEvent event) {
		if (getTeam(event.getPlayer()).equals(getRioters())) {
			event.setCancelled(true);
		} else if (!isBreakable(event.getBlock())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void breakBlock(BlockDamageEvent event) {
		breakBlock(event.getPlayer(), event.getBlock());
	}
	
	@EventHandler
	public void breakBlock(PlayerInteractEvent event) {
		if (!UtilEvent.isBlockClick(event) || !UtilEvent.isLeftClick(event)) {
			return;
		}
		
		breakBlock(event.getPlayer(), event.getClickedBlock());
	}
	
	private void breakBlock(Player player, Block block) {
		if (!isPlaying() || explaining || !canInteract(player)) {
			return;
		}
		
		if (getTeam(player).equals(getRioters())) {
			if (isBreakable(block)) {
				if (Timer.instance.use(player, null, "Break Block", 49L, false)) {
					addRioterPoints(1);
					breakBuffer++;
					UtilEffect.breakBlock(block);
				}
			}
		}
	}
	
	private boolean isBreakable(Block block) {
		return !unbreakable.contains(UtilMat.getData(block));
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (!isPlaying() || explaining || !canInteract(player)) {
			return;
		}
		
		if (getTeam(player).equals(getRioters())) {
			addRioterPoints(1);
		}
	}
	
	@EventHandler
	public void onExplode(EntityExplodeEvent event) {
		if (!isPlaying() || explaining) {
			return;
		}
		
		Iterator<Block> blockIt = event.blockList()
				.iterator();
		while (blockIt.hasNext()) {
			if (!isBreakable(blockIt.next())) {
				blockIt.remove();
			}
		}
		
		int size = event.blockList()
				.size();
				
		explodeBuffer += size;
		addRioterPoints(size);
	}
	
	@EventHandler
	public void onBurn(BlockBurnEvent event) {
		if (event.isCancelled()) {
			return;
		}
		
		if (!isBreakable(event.getBlock())) {
			for (Block fire : UtilBlock.getTouchingBlocks(event.getBlock())) {
				if (fire.getType() == Material.FIRE) {
					fire.setType(Material.AIR);
				}
			}
			event.setCancelled(true);
		} else {
			fireBuffer += 1;
			addRioterPoints(1);
		}
		
	}
	
	
	@EventHandler
	public void onIgnite(BlockIgniteEvent event) {
		Block block = event.getBlock();
		for (Block other : UtilBlock.getTouchingBlocks(block)) {
			if (other.getType()
					.isFlammable() && !isBreakable(other)) {
				event.setCancelled(true);
			}
		}
	}
	
	private int copKillPoints = 30;
	// When 3 or more players kill a cop
	private int mobKillPoints = 50;
	
	@EventHandler
	public void onRioterDeath(CustomDeathEvent event) {
		if (!event.isPlayerKilled()) {
			return;
		}
		
		Player player = event.getKilledPlayer();
		if (!canInteract(player)) {
			return;
		}
		
		GameTeam team = getTeam(player);
		if (team.equals(getCops())) {
			if (event.getAssists()
					.size() >= 2) {
				addRioterPoints(mobKillPoints);
				announceRiotPoints(mobKillPoints, "mob kill");
			} else {
				addRioterPoints(copKillPoints);
				announceRiotPoints(copKillPoints, "cop kill");
			}
		} else {
			addRioterPoints(-10);
		}
	}
	
	@EventHandler
	public void announceDestructionPoints(TickEvent event) {
		if (event.is(Tick.SEC_1)) {
			if (explodeBuffer > 0) {
				announceRiotPoints(explodeBuffer, "explosion");
				explodeBuffer = 0;
			}
		} else if (event.is(Tick.SEC_10)) {
			if (fireBuffer > 0) {
				announceRiotPoints(fireBuffer, "fire");
				fireBuffer = 0;
			}
		} else if (event.is(Tick.SEC_5)) {
			if (breakBuffer > 0) {
				announceRiotPoints(breakBuffer, "destruction");
				breakBuffer = 0;
			}
		}
	}
	
	private void announceRiotPoints(int points, String reason) {
		C.bRaw(C.cRed + "+" + points + C.cGray + " (" + reason + ")");
	}
	
	@Override
	protected List<GameTeam> getWinners() {
		return null;
	}
	
}
