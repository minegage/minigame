package com.lebroncraft.minigame.game.games.riot.kit;


import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.core.common.util.Rand;
import com.lebroncraft.minigame.game.games.riot.GameRiot;
import com.lebroncraft.minigame.kit.Kit;


public class KitRioter
		extends Kit {
		
	public KitRioter() {
		super("Rioter", new String[] { "Receives random items to riot with!" });
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		ItemStack head = null;
		ItemStack chest = null;
		ItemStack legs = null;
		ItemStack boots = null;
		
		double chance = 20;
		if (( (GameRiot) getGame() ).isRiotBoosted()) {
			chance = 40;
		}
		
		if (Rand.rand(chance)) {
			head = new ItemStack(Material.LEATHER_HELMET);
		}
		if (Rand.rand(chance)) {
			chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		}
		if (Rand.rand(chance)) {
			legs = new ItemStack(Material.LEATHER_LEGGINGS);
		}
		if (Rand.rand(chance)) {
			boots = new ItemStack(Material.LEATHER_BOOTS);
		}
		
		inv.setHelmet(head);
		inv.setChestplate(chest);
		inv.setLeggings(legs);
		inv.setBoots(boots);
		
		inv.addItem(getRandLoot());
		inv.addItem(getRandLoot());
		
		for (int i = 0; i < 3; i++) {
			if (Rand.rand(50)) {
				inv.addItem(getRandLoot());
			}
		}
		
	}
	
	private ItemStack getRandLoot() {
		return ( (GameRiot) getGame() ).getRandLoot();
	}
	
}
