package com.lebroncraft.minigame.game.games.riot.kit.attrib;


import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.util.UtilEffect;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.minigame.kit.attrib.Attrib;


public class AttribDefuse
		extends Attrib {
		
	public AttribDefuse() {
		super("Defuser", new String[0]);
	}
	
	@Override
	public void apply(Player player) {
		// Do nothing
	}
	
	@EventHandler
	public void onTntClick(PlayerInteractAtEntityEvent event) {
		ItemStack item = event.getPlayer()
				.getItemInHand();
		if (item == null) {
			return;
		}
		
		if (item.getType() != Material.SHEARS) {
			return;
		}
		
		if (!appliesTo(event.getPlayer())) {
			return;
		}
		
		UtilSound.playPhysical(event.getPlayer()
				.getLocation(), Sound.SHEEP_SHEAR, 1F, 1F);
				
		Entity clicked = event.getRightClicked();
		if (clicked.getType() != EntityType.PRIMED_TNT) {
			return;
		}
		
		UtilSound.playPhysical(clicked.getLocation(), Sound.FIZZ, 1F, 2F);
		UtilEffect.play(clicked.getLocation(), Effect.STEP_SOUND, Material.TNT);
		clicked.remove();
	}
	
}
