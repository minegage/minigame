package com.lebroncraft.minigame.game.games.riot.kit.attrib;


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import com.lebroncraft.core.common.util.UtilBlock;
import com.lebroncraft.core.common.util.UtilEffect;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.games.riot.GameRiot;
import com.lebroncraft.minigame.kit.attrib.Attrib;


public class AttribExtinguish
		extends Attrib {
		
	public AttribExtinguish() {
		super("Extinguish", new String[0]);
	}
	
	@Override
	public void apply(Player player) {
	
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (!UtilEvent.isBlockClick(event)) {
			return;
		}
		
		if (UtilEvent.isRightClick(event)) {
			return;
		}
		
		if (!appliesTo(event.getPlayer())) {
			return;
		}
		
		Block block = event.getClickedBlock();
		
		Game game = getGame();
		if (game instanceof GameRiot) {
			( (GameRiot) game ).addRioterPoints(-1);
		}
		
		if (block.getType() == Material.FIRE) {
			return;
		}
		
		for (Block adj : UtilBlock.getTouchingBlocks(block)) {
			if (adj.getType() == Material.FIRE) {
				UtilEffect.breakBlock(adj);
			}
		}
	}
	
}
