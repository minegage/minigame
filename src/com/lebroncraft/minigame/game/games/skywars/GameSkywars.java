package com.lebroncraft.minigame.game.games.skywars;


import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameFFA;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.kit.Kit;


public class GameSkywars
		extends GameFFA {
		
	protected GameSkywars(MinigameManager manager) {
		super(manager, GameType.SKYWARS, new String[] { }, new Kit[] { });
	}
	
	@Override
	public void loadData(DataFile data) {}
	
	@Override
	public boolean endCheck() {
		return false;
	}
	
}
