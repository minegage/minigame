package com.lebroncraft.minigame.game.games.bountyhunter;


import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.util.UtilTime;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameFFA;
import com.lebroncraft.minigame.game.GameType;


public class GameBountyHunter
		extends GameFFA {
		
	public GameBountyHunter(MinigameManager manager) {
		super(manager, GameType.BOUNTY_HUNTER, new String[] { });
	}
	
	@Override
	public void loadData(DataFile data) {
		// Do nothing
	}
	
	@Override
	public void start() {
		super.start();
		
		runSyncTimer("Bounty Tick", UtilTime.toTicks(explainTime) + 20, UtilTime.toTicks(30.0), new Runnable() {
			@Override
			public void run() {
			
			}
		});
		
	}
	
	
	
}
