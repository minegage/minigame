package com.lebroncraft.minigame.game.games.xpwars;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.menu.Menu;
import com.lebroncraft.core.menu.button.Button;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.Game;


public class EnchantMenu
		extends Menu {
		
	public static final String RAW_NAME = "xpwars menu";
	
	private MinigameManager manager;
	
	public EnchantMenu(MinigameManager manager) {
		super(manager.getMenuManager(), "Enchant your items!", RAW_NAME, null, InventoryType.ENCHANTING);
		this.manager = manager;
		
		// Allow items to be moved into the enchanting area
		this.lockItems = false;
	}
	
	@Override
	public void addComponents() {
		// Make lapis immovable
		addButton(1, new Button() {
			@Override
			public boolean onClick(Player player, Click click) {
				return false;
			}
		});
	}
	
	private final ItemStack LAPIS = UtilItem.create(Material.INK_SACK, 4);
	
	@Override
	public void addItems(Player player, Inventory inventory) {
		// Add lapis to the enchanting inventory
		int lapis = getGame().getStatTracker()
				.get(player, "lapis");
				
		LAPIS.setAmount(lapis);
		
		inventory.setItem(1, LAPIS);
	}
	
	private Game getGame() {
		return manager.getGame();
	}
	
}
