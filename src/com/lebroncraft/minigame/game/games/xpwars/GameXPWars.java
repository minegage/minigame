package com.lebroncraft.minigame.game.games.xpwars;


import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.objective.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.game.GameFFA;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.game.games.xpwars.kits.KitXPWarrior;
import com.lebroncraft.minigame.stats.Stat;
import com.lebroncraft.minigame.stats.UpdatePlayerStatEvent;
import com.lebroncraft.minigame.winnable.PlayerComparator;


public class GameXPWars
		extends GameFFA {
		
	private final int WIN_KILLS = 10;
	
	public ItemStack enchantItem = UtilItem.create(Material.NETHER_STAR, C.cBold + "Enchant your items! " + C.cReset
			+ " (right click)");
			
	private EnchantMenu menu;
	
	public GameXPWars(MinigameManager manager) {
		super(manager, GameType.XPWARS, new String[] { }, new KitXPWarrior());
		
		itemDropDeny.add(enchantItem.getData());
		
		menu = new EnchantMenu(manager);
	}
	
	@Override
	protected Comparator<Player> getWinnerComparator() {
		return new PlayerComparator(stats, Stat.KILLS);
	}
	
	@Override
	public void loadData(DataFile data) {
		// Do nothing
	}
	
	@EventHandler
	public void updateBoard(UpdatePlayerStatEvent event) {
		if (event.getStat()
				.equals("lapis")) {
			Player player = event.getPlayer();
			int lapis = event.getNewValue();
			
			Board board = boardManager.getBoard(player);
			ObjectiveSide side = board.getSideObjective();
			side.updateRow(13, lapis + "");
		}
	}
	
	private int scoreStart;
	
	@Override
	public void giveBoard(Player player, Board board) {
		ObjectiveSide side = board.getSideObjective();
		side.addRow("");
		side.addRow(C.cBlue + C.cBold + "Lapis");
		side.addRow("0");
		side.addRow("");
		side.addRow("First to " + WIN_KILLS);
		side.addRow("");
		
		boardManager.setBoard(player, board);
		
		scoreStart = side.nextRowNum();
		updateBoard(player, board);
	}
	
	@EventHandler
	public void updateScores(UpdatePlayerStatEvent event) {
		if (event.getStat()
				.equals(Stat.KILLS)) {
				
			for (Entry<Player, Board> playerBoard : boardManager.getBoards()
					.entrySet()) {
				updateBoard(playerBoard.getKey(), playerBoard.getValue());
			}
			
			for (Player player : getPlayersIn()) {
				if (stats.get(player, Stat.KILLS) >= WIN_KILLS) {
					setState(GameState.ENDING);
				}
			}
		}
	}
	
	private void updateBoard(Player player, Board board) {
		List<Player> players = getPlayersIn();
		players.sort(new PlayerComparator(stats, Stat.KILLS));
		
		ObjectiveSide side = board.getSideObjective();
		for (int i = scoreStart; i > 0; i--) {
			side.removeRow(i);
		}
		
		Iterator<Player> playersIt = players.iterator();
		while (side.hasRoom() && playersIt.hasNext()) {
			Player next = playersIt.next();
			Integer score = stats.get(next, Stat.KILLS);
			
			if (score > 0) {
				String prefix = "";
				if (player.equals(next)) {
					prefix = C.cPink;
				}
				
				String content = prefix + score + " " + next.getName();
				side.addRow(content);
			}
		}
		
		// Add spacer if there's room
		if (side.hasRoom()) {
			side.addRow("");
		}
	}
	
	public EnchantMenu getMenu() {
		return menu;
	}
	
}
