package com.lebroncraft.minigame.game.games.xpwars.kits;


import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.minigame.game.games.xpwars.GameXPWars;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.kit.attrib.Attrib;


public abstract class XPWarsKitBase
		extends Kit {
		
	public XPWarsKitBase(String name, String[] description, Attrib... attributes) {
		super(name, description, attributes);
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		inv.setItem(8, ( (GameXPWars) getGame() ).enchantItem);
	}
	
}
