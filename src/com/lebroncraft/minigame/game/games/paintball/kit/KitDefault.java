package com.lebroncraft.minigame.game.games.paintball.kit;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.build.ItemBuild;
import com.lebroncraft.core.common.util.UtilArmour;
import com.lebroncraft.core.common.util.UtilArmour.ArmourType;
import com.lebroncraft.minigame.game.games.paintball.AttribPaintballGun;
import com.lebroncraft.minigame.kit.Kit;
import com.lebroncraft.minigame.team.GameTeam;


public class KitDefault
		extends Kit {
		
	public KitDefault() {
		super("Default", new String[] { }, new AttribPaintballGun());
	}
	
	@Override
	protected void giveItems(PlayerInventory inv) {
		GameTeam team = getGame().getTeam((Player) inv.getHolder());
		
		
		Material mat;
		if (team.getName()
				.contains("Red")) {
			mat = Material.SNOW_BALL;
		} else {
			mat = Material.ENDER_PEARL;
		}
		
		ItemStack item = ItemBuild.create(mat)
				.name(team.getPrefix() + C.cBold + "Paintball " + C.cReset + team.getPrefix() + "(click to shoot)")
				.item();
				
		ItemStack[] armour = UtilArmour.getArmourSet(ArmourType.LEATHER);
		colourTeamArmour(inv.getHolder(), armour);
		UtilArmour.equip(inv.getHolder(), armour);
		
		inv.addItem(item);
	}
	
}
