package com.lebroncraft.minigame.game.games.paintball;


import java.util.List;

import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Team;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.objective.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.DataFile;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.minigame.MinigameManager;
import com.lebroncraft.minigame.event.GameDeathEvent;
import com.lebroncraft.minigame.game.GameTDM;
import com.lebroncraft.minigame.game.GameType;
import com.lebroncraft.minigame.game.games.paintball.kit.KitDefault;
import com.lebroncraft.minigame.team.GameTeam;
import com.lebroncraft.minigame.team.JoinTeamEvent;


public class GamePaintball
		extends GameTDM {
		
	public GamePaintball(MinigameManager manager) {
		super(manager, GameType.PAINTBALL, new String[] { }, new KitDefault());
		
		this.timed = true;
		this.timeLimit = 60 * 3 + explainTime;
	}
	
	@Override
	public void loadData(DataFile data) {
		// Do nothing
	}
	
	@Override
	public boolean endCheck() {
		return super.endCheckSurvival();
	}
	
	@Override
	public void createTeams() {
		createTeam("Red", C.cRed).setArmourColour(Color.RED);
		createTeam("Blue", C.cBlue).setArmourColour(Color.BLUE);
	}
	
	private int listStart;
	private int timeRow;
	
	@Override
	public void giveBoard(Player player, Board board) {
		ObjectiveSide side = board.getSideObjective();
		
		side.addRow("");
		timeRow = side.addRow(UtilUI.getTimer((int) getRemainingSeconds()));
		side.addRow("");
		listStart = side.nextRowNum();
		
		for (Team team : board.getBoard()
				.getTeams()) {
			team.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OTHER_TEAMS);
		}
		
		updateBoard(board);
	}
	
	@EventHandler
	public void tickSeconds(TickEvent event) {
		if (event.isNot(Tick.SEC_1)) {
			return;
		}
		
		updateAll();
	}
	
	// Only update the list when needed
	private boolean listUpdate = false;
	
	@SuppressWarnings("unused")
	@EventHandler
	public void joinTeam(JoinTeamEvent event) {
		listUpdate = true;
	}
	
	@SuppressWarnings("unused")
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		listUpdate = true;
	}
	
	@EventHandler
	public void onDead(GameDeathEvent event) {
		if (event.isPlayerOut()) {
			listUpdate = true;
		}
	}
	
	private void updateAll() {
		if (!inLobby()) {
			for (Player player : UtilServer.players()) {
				Board board = boardManager.getBoard(player);
				updateBoard(board);
			}
			listUpdate = false;
		}
	}
	
	private void updateBoard(Board board) {
		if (isPlaying() && !explaining) {
			int remaining = (int) getRemainingSeconds();
			String content = UtilUI.getTimer(remaining);
			
			board.getSideObjective()
					.updateRow(timeRow, content);
		}
		
		// Only update the team list when needed
		if (listUpdate) {
			ObjectiveSide side = board.getSideObjective();
			
			for (int i = listStart; i > 0; i--) {
				side.removeRow(i);
			}
			
			side.addRow(C.cRed + C.cBold + "Red");
			List<Player> red = getTeam("Red").getPlayersIn();
			if (red.size() > 4) {
				side.addRow(C.cRed + red.size() + " alive");
			} else {
				for (Player p : red) {
					side.addRow(C.cRed + p.getName());
				}
			}
			
			side.addRow("");
			side.addRow(C.cAqua + C.cBold + "Blue");
			List<Player> blue = getTeam("Blue").getPlayersIn();
			if (blue.size() > 4) {
				side.addRow(C.cAqua + blue.size() + " alive");
			} else {
				for (Player p : blue) {
					side.addRow(C.cAqua + p.getName());
				}
			}
			
			side.addRow("");
		}
	}
	
	@EventHandler
	public void cancelTeleport(PlayerTeleportEvent event) {
		if (event.getCause() == TeleportCause.ENDER_PEARL) {
			event.setCancelled(true);
		}
	}
	
	@Override
	protected List<GameTeam> getWinners() {
		return super.getWinnersSurvival();
	}
	
}
