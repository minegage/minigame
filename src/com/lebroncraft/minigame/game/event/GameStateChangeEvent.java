package com.lebroncraft.minigame.game.event;


import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.lebroncraft.minigame.game.Game.GameState;


public class GameStateChangeEvent
		extends Event {
		
	private static final HandlerList handlers = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private GameState prevState;
	private GameState newState;
	
	public GameStateChangeEvent(GameState prevState, GameState newState) {
		this.prevState = prevState;
		this.newState = newState;
	}
	
	public GameState getOldState() {
		return prevState;
	}
	
	public GameState getNewState() {
		return newState;
	}
}
