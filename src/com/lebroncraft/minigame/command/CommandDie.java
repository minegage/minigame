package com.lebroncraft.minigame.command;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.rank.Rank;

import net.minecraft.server.v1_8_R2.DamageSource;


public class CommandDie
		extends CommandBase {
		
	public CommandDie() {
		super(Rank.ADMIN, "die");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		UtilEntity.damage(player, 5000, DamageSource.GENERIC);
	}
	
}
