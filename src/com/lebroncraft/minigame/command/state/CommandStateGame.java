package com.lebroncraft.minigame.command.state;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;


public class CommandStateGame
		extends CommandModule<GameManager> {
		
	public CommandStateGame(GameManager manager) {
		super(manager, Rank.ADMIN, "game");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, "State", "Please specify a game state; loading, waiting, starting, playing, ending, or dead");
			return;
		}
		
		GameState state = UtilJava.parseEnum(GameState.class, args.get(0));
		if (state == null) {
			C.pMain(player, "State", "Invalid game state " + C.fElem(args.get(0))
					+ "; must be loading, waiting, starting, playing, ending, or dead");
			return;
		}
		
		Game game = plugin.getGame();
		if (game == null) {
			C.pMain(player, "State", "A game is not currently running");
			return;
		}
		
		game.setState(state);
		C.pMain(player, "State", "Set game state to " + C.fElem(args.get(0)));
	}
}
