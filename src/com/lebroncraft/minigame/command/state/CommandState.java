package com.lebroncraft.minigame.command.state;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.minigame.GameManager;


public class CommandState
		extends CommandModule<GameManager> {
		
	public CommandState(GameManager manager) {
		super(manager, Rank.ADMIN, "state");
		
		addSubCommand(new CommandStateGame(manager));
		addSubCommand(new CommandStatePlayer(manager));
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pHelp(player, "state player <player> <in/out>", "Sets the player state of a player");
		C.pHelp(player, "state game <loading/waiting/starting/playing/ending/dead", "Sets the game state");
	}
	
}
