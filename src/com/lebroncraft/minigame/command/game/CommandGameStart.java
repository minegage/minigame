package com.lebroncraft.minigame.command.game;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;


public class CommandGameStart
		extends CommandGameBase {
		
	public CommandGameStart(GameManager gameManager) {
		super(gameManager, "start");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		Game game = getGame(player);
		if (game == null) {
			return;
		}
		
		if (!game.inLobby()) {
			C.pMain(player, "Game Mgr", "A game is already running");
			return;
		}
		
		game.setState(GameState.PLAYING);
	}
	
}
