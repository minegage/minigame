package com.lebroncraft.minigame.command.game;


import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;


public abstract class CommandGameBase
		extends CommandModule<GameManager> {
		
	public CommandGameBase(GameManager manager, String... aliases) {
		super(manager, Rank.ADMIN, aliases);
	}
	
	protected Game getGame(Player player) {
		Game game = plugin.getGame();
		if (game == null) {
			C.pMain(player, "Game Mgr", "No game is in progress");
		}
		return game;
	}
	
}
