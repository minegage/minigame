package com.lebroncraft.minigame.command.game;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;


public class CommandGamePause
		extends CommandGameBase {
		
	public CommandGamePause(GameManager manager) {
		super(manager, "pause", "unpause", "resume");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		Game game = getGame(player);
		if (game == null) {
			return;
		}
		
		game.paused = !game.paused;
		String paused = ( game.paused ) ? "paused" : "resumed";
		
		C.pMain(player, "Game Manager", "Game " + paused);
	}
	
}
