package com.lebroncraft.minigame.command.game;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.GameType;


public class CommandGameSet
		extends CommandGameBase {
		
	public CommandGameSet(GameManager manager) {
		super(manager, "set");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, "Game Manager", "Please specify a gametype");
			return;
		}
		
		String typeString = Util.joinList(args, " ");
		GameType type = UtilJava.parseEnum(GameType.class, typeString);
		if (type == null) {
			C.pMain(player, "Game Manager", "Gametype " + C.fElem(typeString) + " not found");
			return;
		}
		
		plugin.getGameRotation()
				.clear();
		plugin.getGameRotation()
				.add(type);
				
		plugin.killGame();
		plugin.createGame(type);
		C.bMain("Game", "The game was set to " + C.fElem(type.getName()) + " by " + C.fElem2(player.getName()));
	}
	
}
