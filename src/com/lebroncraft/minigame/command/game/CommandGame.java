package com.lebroncraft.minigame.command.game;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.minigame.GameManager;


public class CommandGame
		extends CommandGameBase {
		
	public CommandGame(GameManager manager) {
		super(manager, "game");
		
		addSubCommand(new CommandGameStart(manager));
		addSubCommand(new CommandGameEnd(manager));
		addSubCommand(new CommandGamePause(manager));
		addSubCommand(new CommandGameSet(manager));
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
	
	}
	
}
