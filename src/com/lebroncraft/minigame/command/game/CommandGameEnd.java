package com.lebroncraft.minigame.command.game;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.minigame.GameManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.game.Game.GameState;


public class CommandGameEnd
		extends CommandGameBase {
		
	public CommandGameEnd(GameManager manager) {
		super(manager, "end", "stop", "kill");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		Game game = getGame(player);
		if (game == null) {
			return;
		}
		
		C.bWarn("Game", "The game was stopped by " + C.fElem(player.getName()));
		
		game.setState(GameState.DEAD);
	}
	
}
