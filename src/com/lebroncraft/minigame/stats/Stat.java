package com.lebroncraft.minigame.stats;


import org.bukkit.event.Listener;


public class Stat
		implements Listener {
		
	public static final String EARNED = "earned";
	public static final String WINS = "wins";
	public static final String LOSSES = "losses";
	public static final String KILLS = "kills";
	public static final String ASSISTS = "assists";
	public static final String DEATHS = "deaths";
	
	private Stat() {
	
	}
	
}
