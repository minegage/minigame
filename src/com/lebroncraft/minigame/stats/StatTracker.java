package com.lebroncraft.minigame.stats;


import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Lists;
import com.lebroncraft.core.combat.KillAssistEvent;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.event.CustomDeathEvent;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.stats.StatManager;
import com.lebroncraft.minigame.game.Game;
import com.lebroncraft.minigame.team.GameTeam;


/**
 * Acts as an interface for StatManager, but also caches stats on a per game basis. Not limited to
 * thing like kills, deaths, etc; this can be used for storing any information relating to a player.
 */
public class StatTracker
		extends PluginModule {
		
	private SafeMap<GameTeam, SafeMap<String, Integer>> teamStats = new SafeMap<>();
	private SafeMap<Player, SafeMap<String, Integer>> playerStats = new SafeMap<>();
	private List<String> sqlStats;
	
	private String table;
	private Game game;
	private boolean recording = false;
	
	public StatTracker(Game game, String table, String... statNames) {
		super("Stat Tracker", game);
		
		this.game = game;
		this.table = table;
		sqlStats = Lists.newArrayList(statNames);
		
		StatManager statManager = StatManager.instance;
		statManager.addTable(table, sqlStats);
	}
	
	@Override
	protected void onEnable() {
		StatManager statManager = StatManager.instance;
		statManager.cancelSaveTask();
		statManager.scheduleSaveTask();
	}
	
	@Override
	protected void onDisable() {
		StatManager statManager = StatManager.instance;
		statManager.cancelSaveTask();
		statManager.removeTable(table);
		playerStats.clear();
		teamStats.clear();
	}
	
	public SafeMap<String, Integer> getStats(GameTeam team) {
		SafeMap<String, Integer> stats = teamStats.get(team);
		if (stats == null) {
			stats = new SafeMap<>();
			teamStats.put(team, stats);
		}
		return stats;
	}
	
	public SafeMap<String, Integer> getStats(Player player) {
		SafeMap<String, Integer> stats = playerStats.get(player);
		if (stats == null) {
			stats = new SafeMap<>();
			playerStats.put(player, stats);
		}
		
		return stats;
	}
	
	public Integer get(Player player, String stat) {
		return getStats(player).getOrDefault(stat, 0);
	}
	
	public Integer get(GameTeam team, String stat) {
		return getStats(team).getOrDefault(stat, 0);
	}
	
	public void set(Player player, String stat, int value) {
		int oldValue = get(player, stat);
		getStats(player).put(stat, value);
		
		UpdatePlayerStatEvent event = new UpdatePlayerStatEvent(player, stat, oldValue, value);
		UtilEvent.call(event);
		
		int difference = value - oldValue;
		GameTeam team = game.getTeam(player);
		increment(team, stat, difference);
	}
	
	public void set(GameTeam team, String stat, int value) {
		int oldValue = get(team, stat);
		
		getStats(team).put(stat, value);
		
		UpdateTeamStatEvent event = new UpdateTeamStatEvent(team, stat, oldValue, value);
		UtilEvent.call(event);
	}
	
	public int increment(Player player, String stat, int increment) {
		int oldValue = get(player, stat);
		int newValue = oldValue + increment;
		set(player, stat, newValue);
		increment(game.getTeam(player), stat, increment);
		return newValue;
	}
	
	public int increment(GameTeam team, String stat, int increment) {
		int value = get(team, stat);
		value += increment;
		set(team, stat, value);
		
		return value;
	}
	
	public void reset(Player player, String stat) {
		int value = get(player, stat);
		increment(player, stat, -value);
	}
	
	public List<String> getSqlStats() {
		return sqlStats;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		playerStats.put(player, new SafeMap<>());
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void clearReference(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		playerStats.remove(player);
	}
	
	private boolean canInteract(Player player) {
		return game.canInteract(player);
	}
	
	/* Default stat tracking */
	@EventHandler(priority = EventPriority.LOWEST)
	public void recordCombat(CustomDeathEvent event) {
		if (!recording || !event.isPlayerKilled()) {
			return;
		}
		
		Player killed = event.getKilledPlayer();
		
		if (canInteract(killed)) {
			increment(killed, Stat.DEATHS, 1);
		}
		
		if (event.isPlayerKiller()) {
			Player pKiller = event.getKillerPlayer();
			increment(pKiller, Stat.KILLS, 1);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void recordAssist(KillAssistEvent event) {
		if (!recording) {
			return;
		}
		OfflinePlayer offPlayer = event.getPlayer();
		if (!offPlayer.isOnline()) {
			return;
		}
		
		Player player = offPlayer.getPlayer();
		increment(player, Stat.ASSISTS, 1);
	}
	
	public void setRecording(boolean recording) {
		this.recording = recording;
	}
	
	public boolean isRecording() {
		return recording;
	}
	
}
